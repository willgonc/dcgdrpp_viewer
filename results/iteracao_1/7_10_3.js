if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_2_3"] = {
	cost: 4957,
	value: 4957,
	executionTime: 180.003,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,4],[4,13],[13,8],[8,14],[14,24],[24,9],[9,40],[40,20],[20,10],[10,13],[13,4],[4,0]],
[[0,4],[4,10],[10,12],[12,48],[48,17],[17,30],[30,19],[19,11],[11,1],[1,13],[13,4],[4,0]],
[[0,4],[4,41],[41,2],[2,28],[28,39],[39,45],[45,3],[3,31],[31,38],[38,43],[43,0]]]
};
