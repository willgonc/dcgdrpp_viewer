if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_2_2"] = {
	cost: 4141,
	value: 4141,
	executionTime: 180.003,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,73],[73,46],[46,28],[28,45],[45,34],[34,58],[58,50],[50,67],[67,26],[26,53],[53,13],[13,31],[31,0]],
[[0,11],[11,62],[62,1],[1,48],[48,63],[63,22],[22,12],[12,57],[57,29],[29,35],[35,24],[24,19],[19,17],[17,32],[32,65],[65,18],[18,64],[64,23],[23,31],[31,0]]]
};
