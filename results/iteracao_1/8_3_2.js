if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_1_2"] = {
	cost: 9451,
	value: 9451,
	executionTime: 193.697,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,70],[70,16],[16,72],[72,23],[23,36],[36,48],[48,53],[53,67],[67,30],[30,53],[53,31],[31,65],[65,29],[29,65],[65,61],[61,47],[47,15],[15,1],[1,55],[55,51],[51,32],[32,55],[55,5],[5,55],[55,57],[57,11],[11,9],[9,43],[43,28],[28,8],[8,64],[64,59],[59,61],[61,0]],
[[0,13],[13,66],[66,44],[44,66],[66,71],[71,46],[46,52],[52,71],[71,73],[73,10],[10,54],[54,68],[68,21],[21,25],[25,18],[18,35],[35,21],[21,26],[26,13],[13,66],[66,34],[34,41],[41,56],[56,58],[58,63],[63,49],[49,6],[6,2],[2,69],[69,42],[42,40],[40,2],[2,13],[13,0]]]
};
