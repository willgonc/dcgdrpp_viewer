if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_20_2_2"] = {
	cost: 5808,
	value: 5808,
	executionTime: 189.074,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,40],[40,19],[19,20],[20,48],[48,27],[27,16],[16,13],[13,14],[14,49],[49,22],[22,11],[11,47],[47,39],[39,37],[37,41],[41,19],[19,0]],
[[0,3],[3,21],[21,25],[25,45],[45,10],[10,17],[17,38],[38,36],[36,46],[46,28],[28,4],[4,31],[31,2],[2,6],[6,12],[12,43],[43,7],[7,9],[9,15],[15,24],[24,0]]]
};
