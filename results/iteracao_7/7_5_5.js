if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_20_3_5"] = {
	cost: 9520,
	value: 9520,
	executionTime: 180.159,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,12],[12,5],[5,22],[22,36],[36,6],[6,9],[9,19],[19,46],[46,6],[6,3],[3,8],[8,13],[13,29],[29,0]],
[[0,12],[12,43],[43,34],[34,18],[18,49],[49,25],[25,13],[13,29],[29,0]],
[[0,12],[12,43],[43,4],[4,33],[33,45],[45,28],[28,7],[7,18],[18,49],[49,24],[24,36],[36,0]],
[[0,35],[35,23],[23,17],[17,2],[2,21],[21,29],[29,0]],
[[0,37],[37,21],[21,44],[44,1],[1,40],[40,11],[11,16],[16,32],[32,14],[14,15],[15,48],[48,20],[20,31],[31,36],[36,0]]]
};
