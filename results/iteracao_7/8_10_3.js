if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_5_2_3"] = {
	cost: 5424,
	value: 5424,
	executionTime: 180.59,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,53],[53,71],[71,5],[5,7],[7,43],[43,14],[14,56],[56,10],[10,58],[58,0]],
[[0,62],[62,20],[20,11],[11,1],[1,37],[37,57],[57,44],[44,45],[45,30],[30,39],[39,60],[60,35],[35,31],[31,8],[8,0]],
[[0,62],[62,20],[20,69],[69,17],[17,61],[61,29],[29,66],[66,67],[67,46],[46,12],[12,63],[63,40],[40,28],[28,41],[41,64],[64,34],[34,53],[53,0]]]
};
