if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_5_1_3"] = {
	cost: 5971,
	value: 5971,
	executionTime: 180.455,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,70],[70,16],[16,19],[19,69],[69,27],[27,63],[63,49],[49,74],[74,56],[56,35],[35,20],[20,21],[21,68],[68,18],[18,62],[62,34],[34,66],[66,0]],
[[0,61],[61,47],[47,5],[5,55],[55,51],[51,55],[55,57],[57,43],[43,64],[64,59],[59,61],[61,0]],
[[0,13],[13,66],[66,71],[71,24],[24,39],[39,31],[31,22],[22,53],[53,30],[30,57],[57,9],[9,8],[8,64],[64,59],[59,61],[61,0]]]
};
