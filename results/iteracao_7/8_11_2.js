if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_5_3_2"] = {
	cost: 4226,
	value: 4226,
	executionTime: 179.187,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,26],[26,38],[38,34],[34,52],[52,2],[2,49],[49,15],[15,22],[22,44],[44,9],[9,3],[3,5],[5,73],[73,8],[8,7],[7,42],[42,55],[55,61],[61,66],[66,56],[56,20],[20,58],[58,0]],
[[0,26],[26,29],[29,51],[51,10],[10,62],[62,43],[43,40],[40,11],[11,4],[4,63],[63,1],[1,45],[45,0]]]
};
