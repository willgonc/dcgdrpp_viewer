if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_2_2"] = {
	cost: 5698,
	value: 5698,
	executionTime: 177.378,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,22],[22,45],[45,40],[40,14],[14,39],[39,5],[5,13],[13,3],[3,36],[36,42],[42,32],[32,34],[34,16],[16,0]],
[[0,6],[6,9],[9,48],[48,2],[2,12],[12,41],[41,33],[33,1],[1,4],[4,25],[25,46],[46,19],[19,26],[26,29],[29,43],[43,31],[31,23],[23,15],[15,20],[20,0]]]
};
