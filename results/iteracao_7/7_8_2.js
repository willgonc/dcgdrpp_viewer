if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_2_3_2"] = {
	cost: 2591,
	value: 2591,
	executionTime: 180,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,42],[42,12],[12,31],[31,7],[7,30],[30,3],[3,28],[28,7],[7,31],[31,12],[12,42],[42,0]],
[[0,41],[41,47],[47,43],[43,49],[49,32],[32,48],[48,4],[4,34],[34,0]]]
};
