if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_3_2"] = {
	cost: 4009,
	value: 4009,
	executionTime: 180.051,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,29],[29,30],[30,5],[5,34],[34,46],[46,3],[3,25],[25,1],[1,36],[36,27],[27,40],[40,20],[20,48],[48,9],[9,42],[42,6],[6,11],[11,0]],
[[0,8],[8,32],[32,22],[22,31],[31,4],[4,44],[44,47],[47,0]]]
};
