if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_2_2_3"] = {
	cost: 3902,
	value: 3902,
	executionTime: 180.002,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,9],[9,1],[1,18],[18,25],[25,19],[19,12],[12,27],[27,0]],
[[0,9],[9,15],[15,7],[7,4],[4,23],[23,21],[21,49],[49,17],[17,9],[9,0]],
[[0,14],[14,42],[42,5],[5,26],[26,27],[27,0]]]
};
