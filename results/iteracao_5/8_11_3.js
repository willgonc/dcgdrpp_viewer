if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_5_3_3"] = {
	cost: 4730,
	value: 4730,
	executionTime: 179.724,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,58],[58,20],[20,56],[56,66],[66,55],[55,61],[61,42],[42,18],[18,47],[47,67],[67,8],[8,47],[47,27],[27,0]],
[[0,26],[26,29],[29,51],[51,10],[10,62],[62,43],[43,40],[40,11],[11,4],[4,63],[63,1],[1,45],[45,0]],
[[0,26],[26,38],[38,34],[34,52],[52,2],[2,49],[49,15],[15,22],[22,44],[44,9],[9,27],[27,0]]]
};
