if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_1_3"] = {
	cost: 6038,
	value: 6038,
	executionTime: 178.576,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,13],[13,34],[34,18],[18,4],[4,20],[20,35],[35,13],[13,0]],
[[0,47],[47,15],[15,1],[1,3],[3,38],[38,45],[45,38],[38,24],[24,46],[46,37],[37,6],[6,49],[49,14],[14,13],[13,0]],
[[0,8],[8,43],[43,9],[9,33],[33,23],[23,36],[36,22],[22,39],[39,29],[29,31],[31,19],[19,42],[42,40],[40,27],[27,13],[13,0]]]
};
