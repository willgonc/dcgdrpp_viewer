if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_1_2"] = {
	cost: 3969,
	value: 3969,
	executionTime: 180.014,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,70],[70,16],[16,72],[72,23],[23,67],[67,53],[53,48],[48,31],[31,39],[39,24],[24,46],[46,60],[60,63],[63,74],[74,56],[56,35],[35,4],[4,18],[18,62],[62,34],[34,66],[66,0]],
[[0,61],[61,47],[47,15],[15,1],[1,55],[55,57],[57,43],[43,64],[64,8],[8,59],[59,61],[61,0]]]
};
