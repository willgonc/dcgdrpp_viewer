if (typeof results == "undefined")
	window.results = {};
results["M5211_gdrpp_23_2"] = {
	cost: 15652,
	value: 15652,
	executionTime: 178.964,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,23],[23,2],[2,3],[3,4],[4,5],[5,192],[192,6],[6,14],[14,15],[15,190],[190,15],[15,14],[14,7],[7,193],[193,11],[11,40],[40,41],[41,43],[43,48],[48,47],[47,46],[46,86],[86,89],[89,88],[88,116],[116,117],[117,87],[87,88],[88,89],[89,90],[90,91],[91,81],[81,189],[189,79],[79,69],[69,68],[68,67],[67,57],[57,33],[33,191],[191,18],[18,4],[4,3],[3,19],[19,20],[20,0]],
[[0,23],[23,32],[32,31],[31,58],[58,59],[59,66],[66,77],[77,76],[76,97],[97,187],[187,106],[106,125],[125,124],[124,144],[144,122],[122,121],[121,120],[120,162],[162,167],[167,168],[168,169],[169,160],[160,173],[173,172],[172,181],[181,179],[179,178],[178,177],[177,176],[176,156],[156,147],[147,139],[139,138],[138,136],[136,135],[135,133],[133,132],[132,186],[186,128],[128,103],[103,102],[102,101],[101,70],[70,28],[28,29],[29,30],[30,31],[31,32],[32,0]]]
};
