if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_1_2"] = {
	cost: 9633,
	value: 9633,
	executionTime: 214.813,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,70],[70,42],[42,27],[27,63],[63,6],[6,2],[2,14],[14,34],[34,62],[62,25],[25,68],[68,21],[21,26],[26,13],[13,66],[66,44],[44,3],[3,55],[55,51],[51,50],[50,55],[55,57],[57,11],[11,9],[9,43],[43,28],[28,8],[8,64],[64,59],[59,47],[47,15],[15,47],[47,61],[61,0]],
[[0,13],[13,49],[49,6],[6,74],[74,41],[41,74],[74,58],[58,10],[10,54],[54,20],[20,35],[35,4],[4,25],[25,38],[38,66],[66,71],[71,73],[73,71],[71,46],[46,52],[52,42],[42,69],[69,19],[19,72],[72,23],[23,36],[36,48],[48,53],[53,30],[30,67],[67,53],[53,31],[31,65],[65,29],[29,65],[65,61],[61,0]]]
};
