if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_20_3_4"] = {
	cost: 8656,
	value: 8656,
	executionTime: 180.159,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,12],[12,5],[5,22],[22,36],[36,6],[6,9],[9,19],[19,46],[46,6],[6,3],[3,8],[8,13],[13,29],[29,0]],
[[0,12],[12,43],[43,20],[20,48],[48,24],[24,49],[49,25],[25,13],[13,29],[29,37],[37,0]],
[[0,12],[12,5],[5,33],[33,45],[45,39],[39,28],[28,7],[7,18],[18,24],[24,36],[36,0]],
[[0,35],[35,23],[23,17],[17,2],[2,21],[21,44],[44,1],[1,40],[40,11],[11,16],[16,32],[32,14],[14,15],[15,31],[31,36],[36,0]]]
};
