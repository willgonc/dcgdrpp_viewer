if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_3_2"] = {
	cost: 3450,
	value: 3450,
	executionTime: 180.854,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,50],[50,71],[71,24],[24,72],[72,10],[10,3],[3,4],[4,61],[61,40],[40,8],[8,6],[6,2],[2,0]],
[[0,26],[26,14],[14,18],[18,68],[68,17],[17,59],[59,70],[70,37],[37,21],[21,67],[67,42],[42,22],[22,57],[57,27],[27,0]]]
};
