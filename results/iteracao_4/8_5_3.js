if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_3_3"] = {
	cost: 7655,
	value: 7655,
	executionTime: 182.975,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,6],[6,2],[2,30],[30,51],[51,22],[22,24],[24,23],[23,35],[35,36],[36,52],[52,43],[43,37],[37,57],[57,29],[29,37],[37,34],[34,0]],
[[0,53],[53,10],[10,42],[42,26],[26,25],[25,14],[14,42],[42,63],[63,34],[34,68],[68,18],[18,47],[47,33],[33,73],[73,28],[28,41],[41,62],[62,71],[71,65],[65,58],[58,19],[19,58],[58,5],[5,70],[70,9],[9,68],[68,0]],
[[0,27],[27,2],[2,38],[38,67],[67,60],[60,4],[4,31],[31,69],[69,4],[4,49],[49,60],[60,13],[13,48],[48,40],[40,21],[21,46],[46,11],[11,7],[7,59],[59,15],[15,55],[55,44],[44,0]]]
};
