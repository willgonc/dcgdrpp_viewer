if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_2_2"] = {
	cost: 5801,
	value: 5801,
	executionTime: 182.738,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,22],[22,45],[45,10],[10,34],[34,32],[32,44],[44,13],[13,3],[3,11],[11,18],[18,15],[15,20],[20,0]],
[[0,6],[6,2],[2,48],[48,17],[17,21],[21,24],[24,35],[35,12],[12,41],[41,33],[33,1],[1,4],[4,7],[7,43],[43,29],[29,38],[38,19],[19,28],[28,20],[20,0]]]
};
