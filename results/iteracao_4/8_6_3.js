if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_1_3"] = {
	cost: 4649,
	value: 4649,
	executionTime: 180.002,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,14],[14,62],[62,4],[4,18],[18,62],[62,34],[34,66],[66,0]],
[[0,61],[61,47],[47,15],[15,1],[1,55],[55,57],[57,43],[43,64],[64,8],[8,59],[59,61],[61,0]],
[[0,70],[70,16],[16,72],[72,23],[23,67],[67,53],[53,48],[48,31],[31,39],[39,24],[24,46],[46,60],[60,49],[49,6],[6,2],[2,13],[13,0]]]
};
