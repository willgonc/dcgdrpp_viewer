if (typeof results == "undefined")
	window.results = {};
results["A5205_29_gdrpp_3"] = {
	cost: 5566,
	value: 5566,
	executionTime: 179.995,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,26],[26,21],[21,19],[19,15],[15,14],[14,9],[9,6],[6,5],[5,4],[4,7],[7,8],[8,9],[9,2],[2,1],[1,28],[28,1],[1,2],[2,9],[9,15],[15,19],[19,21],[21,26],[26,0]],
[[0,26],[26,33],[33,36],[36,43],[43,47],[47,86],[86,85],[85,88],[88,95],[95,94],[94,96],[96,66],[66,67],[67,106],[106,107],[107,104],[104,102],[102,99],[99,90],[90,87],[87,48],[48,45],[45,38],[38,27],[27,23],[23,26],[26,0]],
[[0,25],[25,24],[24,30],[30,35],[35,40],[40,39],[39,55],[55,62],[62,63],[63,62],[62,64],[64,70],[70,71],[71,72],[72,76],[76,75],[75,80],[80,81],[81,82],[82,81],[81,80],[80,77],[77,80],[80,75],[75,70],[70,64],[64,62],[62,54],[54,61],[61,54],[54,62],[62,55],[55,39],[39,40],[40,35],[35,30],[30,31],[31,32],[32,0]]]
};
