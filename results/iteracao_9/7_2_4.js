if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_3_4"] = {
	cost: 5948,
	value: 5948,
	executionTime: 177.686,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,5],[5,25],[25,18],[18,1],[1,11],[11,15],[15,2],[2,41],[41,0]],
[[0,17],[17,33],[33,3],[3,4],[4,49],[49,43],[43,23],[23,47],[47,32],[32,0]],
[[0,41],[41,30],[30,20],[20,21],[21,8],[8,21],[21,44],[44,28],[28,22],[22,41],[41,0]],
[[0,32],[32,24],[24,9],[9,10],[10,39],[39,40],[40,39],[39,46],[46,17],[17,0]]]
};
