if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_5_3_2"] = {
	cost: 4364,
	value: 4364,
	executionTime: 178.587,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,26],[26,29],[29,51],[51,10],[10,62],[62,43],[43,40],[40,11],[11,4],[4,63],[63,1],[1,45],[45,0]],
[[0,58],[58,20],[20,56],[56,66],[66,55],[55,61],[61,42],[42,18],[18,47],[47,67],[67,8],[8,3],[3,57],[57,14],[14,44],[44,22],[22,72],[72,64],[64,49],[49,2],[2,52],[52,34],[34,38],[38,29],[29,26],[26,0]]]
};
