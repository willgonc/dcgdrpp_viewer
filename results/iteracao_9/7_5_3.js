if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_20_3_3"] = {
	cost: 8112,
	value: 8112,
	executionTime: 184.067,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,37],[37,8],[8,6],[6,9],[9,19],[19,47],[47,17],[17,35],[35,23],[23,17],[17,2],[2,21],[21,29],[29,0]],
[[0,12],[12,5],[5,33],[33,45],[45,39],[39,28],[28,7],[7,18],[18,24],[24,49],[49,25],[25,44],[44,1],[1,21],[21,29],[29,0]],
[[0,12],[12,43],[43,20],[20,48],[48,31],[31,36],[36,22],[22,48],[48,15],[15,14],[14,32],[32,11],[11,41],[41,36],[36,6],[6,3],[3,29],[29,0]]]
};
