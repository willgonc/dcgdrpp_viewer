if (typeof results == "undefined")
	window.results = {};
results["M5101_gdrpp_75_2"] = {
	cost: 6890,
	value: 6890,
	executionTime: 180.007,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,73],[73,72],[72,103],[103,128],[128,186],[186,132],[132,133],[133,135],[135,138],[138,139],[139,140],[140,139],[139,147],[147,156],[156,176],[176,177],[177,178],[178,179],[179,180],[180,182],[182,181],[181,172],[172,173],[173,160],[160,161],[161,162],[162,163],[163,166],[166,165],[165,164],[164,163],[163,119],[119,112],[112,110],[110,109],[109,107],[107,106],[106,187],[187,105],[105,98],[98,75],[75,0]],
[[0,75],[75,76],[76,77],[77,78],[78,79],[79,80],[80,81],[81,90],[90,92],[92,114],[114,115],[115,116],[116,88],[88,89],[89,86],[86,46],[46,47],[47,48],[48,49],[49,40],[40,11],[11,194],[194,8],[8,9],[9,44],[44,43],[43,41],[41,42],[42,44],[44,43],[43,41],[41,40],[40,49],[49,51],[51,54],[54,55],[55,56],[56,57],[57,33],[33,191],[191,18],[18,4],[4,3],[3,2],[2,23],[23,24],[24,1],[1,2],[2,21],[21,22],[22,32],[32,31],[31,30],[30,29],[29,28],[28,61],[61,62],[62,63],[63,188],[188,0]]]
};
