if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_1_2"] = {
	cost: 6358,
	value: 6358,
	executionTime: 181.671,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,70],[70,40],[40,27],[27,52],[52,46],[46,37],[37,63],[63,49],[49,6],[6,74],[74,56],[56,35],[35,20],[20,21],[21,68],[68,18],[18,62],[62,34],[34,14],[14,34],[34,66],[66,71],[71,24],[24,29],[29,65],[65,61],[61,0]],
[[0,47],[47,15],[15,1],[1,51],[51,55],[55,5],[5,55],[55,57],[57,11],[11,9],[9,43],[43,28],[28,33],[33,30],[30,53],[53,67],[67,36],[36,23],[23,72],[72,19],[19,69],[69,13],[13,0]]]
};
