if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_3_5"] = {
	cost: 9711,
	value: 9711,
	executionTime: 196.334,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,10],[10,25],[25,52],[52,30],[30,4],[4,23],[23,46],[46,5],[5,13],[13,68],[68,10],[10,0]],
[[0,65],[65,49],[49,7],[7,36],[36,60],[60,17],[17,48],[48,42],[42,56],[56,59],[59,3],[3,10],[10,0]],
[[0,10],[10,47],[47,1],[1,45],[45,37],[37,11],[11,61],[61,74],[74,58],[58,73],[73,25],[25,10],[10,0]],
[[0,10],[10,25],[25,52],[52,53],[53,6],[6,72],[72,28],[28,31],[31,35],[35,50],[50,35],[35,26],[26,27],[27,9],[9,18],[18,0]],
[[0,64],[64,66],[66,29],[29,12],[12,44],[44,62],[62,43],[43,34],[34,26],[26,27],[27,18],[18,0]]]
};
