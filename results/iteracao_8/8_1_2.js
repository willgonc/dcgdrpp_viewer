if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_2_2"] = {
	cost: 8338,
	value: 8338,
	executionTime: 180.47,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,62],[62,56],[56,72],[72,26],[26,19],[19,15],[15,71],[71,55],[55,8],[8,58],[58,37],[37,65],[65,3],[3,49],[49,66],[66,64],[64,47],[47,51],[51,16],[16,67],[67,68],[68,38],[38,68],[68,7],[7,60],[60,0]],
[[0,60],[60,6],[6,27],[27,54],[54,23],[23,4],[4,28],[28,14],[14,52],[52,69],[69,13],[13,73],[73,2],[2,53],[53,2],[2,31],[31,30],[30,65],[65,22],[22,45],[45,61],[61,24],[24,70],[70,5],[5,36],[36,34],[34,17],[17,34],[34,35],[35,29],[29,42],[42,0]]]
};
