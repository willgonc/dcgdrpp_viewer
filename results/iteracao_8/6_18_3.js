if (typeof results == "undefined")
	window.results = {};
results["M5201_gdrpp_147_3"] = {
	cost: 6607,
	value: 6607,
	executionTime: 179.025,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,142],[142,126],[126,105],[105,98],[98,75],[75,65],[65,59],[59,58],[58,31],[31,32],[32,22],[22,21],[21,19],[19,20],[20,18],[18,17],[17,5],[5,192],[192,16],[16,15],[15,190],[190,15],[15,14],[14,13],[13,12],[12,11],[11,40],[40,41],[41,40],[40,49],[49,51],[51,54],[54,82],[82,81],[81,189],[189,95],[95,109],[109,107],[107,124],[124,125],[125,143],[143,142],[142,0]],
[[0,150],[150,154],[154,159],[159,158],[158,173],[173,172],[172,175],[175,178],[178,179],[179,181],[181,172],[172,173],[173,158],[158,159],[159,154],[154,150],[150,0]],
[[0,150],[150,154],[154,159],[159,183],[183,155],[155,160],[160,169],[169,160],[160,161],[161,162],[162,120],[120,121],[121,112],[112,113],[113,114],[114,115],[115,116],[116,117],[117,87],[87,88],[88,89],[89,90],[90,81],[81,80],[80,79],[79,78],[78,77],[77,76],[76,97],[97,187],[187,105],[105,104],[104,127],[127,136],[136,138],[138,139],[139,147],[147,148],[148,145],[145,0]]]
};
