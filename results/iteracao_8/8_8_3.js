if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_3_3"] = {
	cost: 4395,
	value: 4395,
	executionTime: 180.185,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,53],[53,63],[63,4],[4,61],[61,40],[40,8],[8,7],[7,60],[60,57],[57,42],[42,22],[22,57],[57,27],[27,0]],
[[0,52],[52,14],[14,18],[18,68],[68,17],[17,59],[59,70],[70,37],[37,21],[21,67],[67,33],[33,27],[27,0]],
[[0,50],[50,71],[71,24],[24,72],[72,10],[10,3],[3,66],[66,35],[35,0]]]
};
