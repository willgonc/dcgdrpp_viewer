if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_2_2"] = {
	cost: 9599,
	value: 9599,
	executionTime: 181.887,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,56],[56,49],[49,40],[40,46],[46,47],[47,35],[35,63],[63,41],[41,33],[33,38],[38,20],[20,42],[42,45],[45,73],[73,24],[24,61],[61,50],[50,11],[11,10],[10,13],[13,43],[43,69],[69,14],[14,5],[5,25],[25,40],[40,22],[22,0]],
[[0,51],[51,72],[72,30],[30,71],[71,19],[19,16],[16,1],[1,74],[74,58],[58,24],[24,28],[28,23],[23,59],[59,36],[36,29],[29,66],[66,27],[27,67],[67,66],[66,2],[2,29],[29,68],[68,53],[53,52],[52,4],[4,57],[57,37],[37,62],[62,12],[12,65],[65,26],[26,54],[54,60],[60,28],[28,34],[34,9],[9,35],[35,0]]]
};
