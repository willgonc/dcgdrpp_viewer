if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_1_3"] = {
	cost: 4665,
	value: 4665,
	executionTime: 180.365,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,16],[16,23],[23,36],[36,22],[22,39],[39,24],[24,46],[46,49],[49,6],[6,27],[27,40],[40,2],[2,13],[13,0]],
[[0,28],[28,43],[43,12],[12,47],[47,15],[15,0]],
[[0,47],[47,1],[1,38],[38,45],[45,18],[18,20],[20,35],[35,4],[4,34],[34,14],[14,13],[13,0]]]
};
