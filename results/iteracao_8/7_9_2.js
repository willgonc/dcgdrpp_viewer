if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_1_2"] = {
	cost: 4054,
	value: 4054,
	executionTime: 180.326,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,8],[8,43],[43,9],[9,33],[33,23],[23,36],[36,22],[22,39],[39,24],[24,46],[46,27],[27,42],[42,40],[40,27],[27,49],[49,6],[6,14],[14,13],[13,0]],
[[0,47],[47,15],[15,1],[1,38],[38,45],[45,18],[18,20],[20,35],[35,4],[4,34],[34,13],[13,0]]]
};
