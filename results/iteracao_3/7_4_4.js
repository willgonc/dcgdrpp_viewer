if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_20_2_4"] = {
	cost: 7507,
	value: 14467,
	executionTime: 193.34,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,21],[21,45],[45,10],[10,17],[17,38],[38,36],[36,46],[46,28],[28,4],[4,31],[31,42],[42,15],[15,24],[24,0]],
[[0,24],[24,19],[19,20],[20,11],[11,47],[47,39],[39,37],[37,41],[41,0]],
[[0,40],[40,41],[41,19],[19,9],[9,6],[6,12],[12,43],[43,42],[42,15],[15,24],[24,0]],
[[0,21],[21,44],[44,49],[49,22],[22,48],[48,27],[27,16],[16,13],[13,14],[14,49],[49,44],[44,21],[21,30],[30,8],[8,0]]]
};
