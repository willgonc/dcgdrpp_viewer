if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_3_3"] = {
	cost: 4843,
	value: 4843,
	executionTime: 177.802,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,5],[5,18],[18,34],[34,22],[22,28],[28,37],[37,0]],
[[0,33],[33,24],[24,9],[9,10],[10,39],[39,40],[40,14],[14,20],[20,21],[21,8],[8,21],[21,33],[33,17],[17,0]],
[[0,32],[32,47],[47,23],[23,43],[43,49],[49,29],[29,36],[36,15],[15,2],[2,41],[41,0]]]
};
