if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_3_4"] = {
	cost: 9185,
	value: 9185,
	executionTime: 183.737,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,63],[63,26],[26,42],[42,56],[56,59],[59,56],[56,48],[48,17],[17,3],[3,10],[10,0]],
[[0,10],[10,25],[25,52],[52,53],[53,6],[6,72],[72,28],[28,31],[31,35],[35,50],[50,35],[35,26],[26,27],[27,18],[18,0]],
[[0,65],[65,49],[49,36],[36,7],[7,67],[67,66],[66,29],[29,12],[12,44],[44,62],[62,43],[43,34],[34,26],[26,27],[27,9],[9,18],[18,0]],
[[0,10],[10,25],[25,52],[52,30],[30,4],[4,23],[23,46],[46,5],[5,13],[13,1],[1,47],[47,1],[1,45],[45,37],[37,11],[11,61],[61,74],[74,58],[58,73],[73,25],[25,10],[10,0]]]
};
