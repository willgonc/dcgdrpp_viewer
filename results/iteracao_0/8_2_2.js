if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_3_2"] = {
	cost: 9168,
	value: 9168,
	executionTime: 188.063,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,63],[63,26],[26,42],[42,56],[56,38],[38,17],[17,27],[27,9],[9,71],[71,49],[49,36],[36,7],[7,66],[66,12],[12,44],[44,62],[62,43],[43,34],[34,74],[74,58],[58,73],[73,52],[52,30],[30,4],[4,23],[23,46],[46,5],[5,45],[45,47],[47,68],[68,10],[10,0]],
[[0,64],[64,66],[66,29],[29,39],[39,69],[69,37],[37,11],[11,61],[61,74],[74,15],[15,53],[53,6],[6,72],[72,28],[28,31],[31,35],[35,50],[50,35],[35,26],[26,42],[42,48],[48,3],[3,10],[10,0]]]
};
