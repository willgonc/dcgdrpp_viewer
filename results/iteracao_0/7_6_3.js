if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_2_1_3"] = {
	cost: 3942,
	value: 3942,
	executionTime: 179.997,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,28],[28,9],[9,43],[43,33],[33,23],[23,19],[19,42],[42,2],[2,13],[13,0]],
[[0,15],[15,1],[1,15],[15,0]],
[[0,13],[13,2],[2,49],[49,6],[6,37],[37,46],[46,24],[24,10],[10,20],[20,35],[35,13],[13,0]]]
};
