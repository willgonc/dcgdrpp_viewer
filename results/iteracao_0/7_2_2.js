if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_3_2"] = {
	cost: 4238,
	value: 4238,
	executionTime: 178.876,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,32],[32,47],[47,23],[23,43],[43,49],[49,29],[29,36],[36,15],[15,2],[2,41],[41,0]],
[[0,17],[17,33],[33,24],[24,9],[9,10],[10,39],[39,40],[40,14],[14,20],[20,21],[21,8],[8,21],[21,44],[44,28],[28,22],[22,25],[25,18],[18,5],[5,41],[41,0]]]
};
