if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_3_4"] = {
	cost: 5345,
	value: 5345,
	executionTime: 180.001,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,8],[8,32],[32,22],[22,31],[31,4],[4,0]],
[[0,11],[11,19],[19,12],[12,15],[15,40],[40,20],[20,48],[48,9],[9,6],[6,11],[11,0]],
[[0,26],[26,44],[44,47],[47,0]],
[[0,29],[29,30],[30,5],[5,34],[34,46],[46,3],[3,25],[25,18],[18,0]]]
};
