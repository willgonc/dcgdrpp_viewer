if (typeof results == "undefined")
	window.results = {};
results["M5203_gdrpp_104_2"] = {
	cost: 5587,
	value: 5587,
	executionTime: 180.009,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,104],[104,127],[127,136],[136,138],[138,139],[139,147],[147,156],[156,176],[176,177],[177,178],[178,179],[179,181],[181,172],[172,173],[173,160],[160,169],[169,160],[160,161],[161,162],[162,120],[120,121],[121,122],[122,144],[144,124],[124,125],[125,126],[126,104],[104,0]],
[[0,72],[72,73],[73,74],[74,75],[75,65],[65,59],[59,58],[58,31],[31,32],[32,22],[22,21],[21,19],[19,20],[20,18],[18,17],[17,5],[5,192],[192,16],[16,15],[15,190],[190,15],[15,14],[14,13],[13,12],[12,11],[11,40],[40,49],[49,48],[48,47],[47,46],[46,86],[86,89],[89,88],[88,87],[87,117],[117,116],[116,115],[115,114],[114,113],[113,93],[93,94],[94,95],[95,109],[109,107],[107,106],[106,187],[187,105],[105,104],[104,0]]]
};
