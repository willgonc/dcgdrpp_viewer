if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_3_2"] = {
	cost: 7007,
	value: 7007,
	executionTime: 180.401,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,6],[6,2],[2,38],[38,20],[20,48],[48,13],[13,49],[49,4],[4,31],[31,69],[69,4],[4,49],[49,60],[60,67],[67,51],[51,22],[22,24],[24,23],[23,35],[35,36],[36,52],[52,43],[43,37],[37,34],[34,0]],
[[0,34],[34,9],[9,19],[19,58],[58,70],[70,5],[5,71],[71,62],[62,41],[41,65],[65,28],[28,65],[65,73],[73,33],[33,45],[45,57],[57,29],[29,18],[18,68],[68,34],[34,63],[63,42],[42,14],[14,26],[26,25],[25,59],[59,7],[7,21],[21,40],[40,46],[46,11],[11,59],[59,15],[15,55],[55,10],[10,53],[53,44],[44,0]]]
};
