if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_2_1_2"] = {
	cost: 3521,
	value: 3521,
	executionTime: 180.08,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,28],[28,43],[43,17],[17,15],[15,1],[1,15],[15,0]],
[[0,16],[16,42],[42,19],[19,23],[23,22],[22,39],[39,24],[24,46],[46,37],[37,41],[41,35],[35,20],[20,35],[35,13],[13,0]]]
};
