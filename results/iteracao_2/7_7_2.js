if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_2_2_2"] = {
	cost: 3383,
	value: 3383,
	executionTime: 179.997,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,9],[9,39],[39,1],[1,18],[18,25],[25,48],[48,46],[46,13],[13,23],[23,28],[28,17],[17,9],[9,0]],
[[0,14],[14,42],[42,5],[5,26],[26,27],[27,0]]]
};
