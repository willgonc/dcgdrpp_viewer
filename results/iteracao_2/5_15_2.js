if (typeof results == "undefined")
	window.results = {};
results["A5107_91_gdrpp_2"] = {
	cost: 3036,
	value: 3036,
	executionTime: 179.998,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,99],[99,98],[98,97],[97,95],[95,88],[88,85],[85,86],[86,47],[47,46],[46,41],[41,40],[40,39],[39,29],[29,11],[11,12],[12,7],[7,4],[4,7],[7,12],[12,11],[11,2],[2,9],[9,14],[14,18],[18,19],[19,20],[20,17],[17,10],[10,9],[9,14],[14,18],[18,24],[24,25],[25,28],[28,26],[26,23],[23,27],[27,38],[38,45],[45,48],[48,87],[87,0]],
[[0,99],[99,102],[102,104],[104,107],[107,109],[109,110],[110,109],[109,111],[111,115],[115,114],[114,113],[113,111],[111,109],[109,107],[107,106],[106,105],[105,68],[68,105],[105,82],[82,81],[81,80],[80,77],[77,80],[80,75],[75,76],[76,75],[75,70],[70,71],[71,65],[65,56],[56,83],[83,84],[84,92],[92,94],[94,96],[96,97],[97,95],[95,88],[88,85],[85,86],[86,47],[47,48],[48,87],[87,0]]]
};
