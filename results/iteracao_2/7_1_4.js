if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_2_4"] = {
	cost: 7172,
	value: 7172,
	executionTime: 186.009,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,22],[22,45],[45,10],[10,5],[5,13],[13,41],[41,33],[33,8],[8,0]],
[[0,20],[20,15],[15,18],[18,11],[11,36],[36,42],[42,32],[32,34],[34,16],[16,0]],
[[0,20],[20,4],[4,7],[7,43],[43,29],[29,38],[38,19],[19,28],[28,20],[20,0]],
[[0,6],[6,2],[2,48],[48,17],[17,21],[21,24],[24,35],[35,12],[12,41],[41,27],[27,33],[33,8],[8,0]]]
};
