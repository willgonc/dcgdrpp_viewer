if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_20_3_4"] = {
	cost: 7782,
	value: 7782,
	executionTime: 180.981,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,34],[34,63],[63,42],[42,14],[14,59],[59,7],[7,11],[11,46],[46,21],[21,40],[40,25],[25,26],[26,15],[15,55],[55,10],[10,53],[53,44],[44,0]],
[[0,34],[34,68],[68,18],[18,29],[29,57],[57,47],[47,45],[45,33],[33,12],[12,19],[19,70],[70,5],[5,71],[71,62],[62,41],[41,65],[65,58],[58,9],[9,68],[68,0]],
[[0,6],[6,2],[2,38],[38,67],[67,60],[60,4],[4,31],[31,69],[69,4],[4,49],[49,60],[60,13],[13,55],[55,44],[44,0]],
[[0,27],[27,2],[2,30],[30,51],[51,22],[22,24],[24,23],[23,35],[35,36],[36,52],[52,43],[43,37],[37,34],[34,0]]]
};
