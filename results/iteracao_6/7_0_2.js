if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_1_2"] = {
	cost: 5256,
	value: 5256,
	executionTime: 180.329,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,47],[47,15],[15,1],[1,3],[3,38],[38,45],[45,38],[38,24],[24,39],[39,29],[29,24],[24,46],[46,37],[37,49],[49,14],[14,13],[13,0]],
[[0,8],[8,43],[43,9],[9,33],[33,23],[23,19],[19,42],[42,27],[27,49],[49,41],[41,35],[35,20],[20,4],[4,18],[18,25],[25,34],[34,13],[13,0]]]
};
