if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_1_3"] = {
	cost: 5945,
	value: 5945,
	executionTime: 177.817,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,16],[16,23],[23,36],[36,22],[22,39],[39,29],[29,24],[24,46],[46,37],[37,6],[6,41],[41,35],[35,20],[20,4],[4,18],[18,25],[25,34],[34,13],[13,0]],
[[0,47],[47,15],[15,1],[1,3],[3,38],[38,45],[45,38],[38,24],[24,46],[46,27],[27,40],[40,2],[2,13],[13,0]],
[[0,28],[28,43],[43,12],[12,47],[47,15],[15,0]]]
};
