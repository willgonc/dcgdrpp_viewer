if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_10_3_3"] = {
	cost: 8636,
	value: 8636,
	executionTime: 180.077,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,68],[68,13],[13,45],[45,1],[1,47],[47,37],[37,11],[11,61],[61,74],[74,58],[58,32],[32,33],[33,42],[42,56],[56,38],[38,17],[17,27],[27,9],[9,18],[18,0]],
[[0,65],[65,49],[49,36],[36,7],[7,67],[67,66],[66,29],[29,12],[12,44],[44,50],[50,62],[62,48],[48,3],[3,10],[10,0]],
[[0,68],[68,41],[41,22],[22,52],[52,30],[30,46],[46,23],[23,4],[4,20],[20,6],[6,28],[28,31],[31,35],[35,26],[26,27],[27,18],[18,0]]]
};
