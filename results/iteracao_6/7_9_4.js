if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_5_1_4"] = {
	cost: 5290,
	value: 5290,
	executionTime: 180.019,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,13],[13,14],[14,41],[41,4],[4,20],[20,18],[18,25],[25,34],[34,13],[13,0]],
[[0,47],[47,1],[1,38],[38,45],[45,44],[44,0]],
[[0,16],[16,23],[23,36],[36,22],[22,39],[39,24],[24,46],[46,27],[27,40],[40,2],[2,13],[13,0]],
[[0,28],[28,43],[43,12],[12,47],[47,15],[15,0]]]
};
