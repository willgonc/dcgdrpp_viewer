if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_2_4"] = {
	cost: 6005,
	value: 6005,
	executionTime: 179.998,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,73],[73,46],[46,28],[28,45],[45,34],[34,58],[58,50],[50,67],[67,26],[26,53],[53,13],[13,31],[31,0]],
[[0,4],[4,12],[12,57],[57,29],[29,35],[35,66],[66,25],[25,9],[9,23],[23,31],[31,0]],
[[0,44],[44,31],[31,23],[23,64],[64,18],[18,52],[52,65],[65,51],[51,54],[54,32],[32,44],[44,0]],
[[0,11],[11,62],[62,1],[1,48],[48,63],[63,15],[15,55],[55,0]]]
};
