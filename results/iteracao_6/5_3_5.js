if (typeof results == "undefined")
	window.results = {};
results["A3107_55_gdrpp_5"] = {
	cost: 5402,
	value: 5402,
	executionTime: 179.989,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,62],[62,64],[64,65],[65,56],[56,83],[83,84],[84,85],[85,86],[86,87],[87,48],[48,43],[43,36],[36,33],[33,26],[26,21],[21,19],[19,20],[20,17],[17,20],[20,19],[19,18],[18,24],[24,31],[31,32],[32,33],[33,26],[26,23],[23,27],[27,38],[38,45],[45,37],[37,36],[36,35],[35,40],[40,83],[83,56],[56,55],[55,62],[62,0]],
[[0,61],[61,60],[60,59],[59,51],[51,50],[50,58],[58,59],[59,51],[51,50],[50,49],[49,57],[57,62],[62,63],[63,62],[62,0]],
[[0,62],[62,55],[55,39],[39,29],[29,11],[11,12],[12,13],[13,8],[8,9],[9,14],[14,13],[13,8],[8,9],[9,6],[6,3],[3,6],[6,5],[5,8],[8,9],[9,2],[2,1],[1,2],[2,11],[11,29],[29,39],[39,55],[55,62],[62,0]],
[[0,62],[62,64],[64,65],[65,56],[56,83],[83,84],[84,85],[85,86],[86,89],[89,88],[88,95],[95,98],[98,99],[99,102],[102,104],[104,107],[107,109],[109,111],[111,115],[115,114],[114,115],[115,111],[111,109],[109,107],[107,106],[106,105],[105,68],[68,93],[93,83],[83,56],[56,55],[55,62],[62,0]],
[[0,62],[62,64],[64,65],[65,56],[56,83],[83,84],[84,85],[85,86],[86,89],[89,88],[88,95],[95,94],[94,96],[96,66],[66,67],[67,106],[106,105],[105,82],[82,81],[81,78],[78,79],[79,69],[69,70],[70,75],[75,76],[76,75],[75,70],[70,64],[64,62],[62,0]]]
};
