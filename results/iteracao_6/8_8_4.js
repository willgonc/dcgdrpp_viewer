if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_75_3_2_3_4"] = {
	cost: 5279,
	value: 5279,
	executionTime: 180.027,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,53],[53,63],[63,4],[4,61],[61,40],[40,8],[8,6],[6,2],[2,0]],
[[0,52],[52,14],[14,18],[18,43],[43,55],[55,1],[1,70],[70,59],[59,17],[17,43],[43,18],[18,52],[52,0]],
[[0,50],[50,71],[71,24],[24,72],[72,10],[10,3],[3,66],[66,35],[35,0]],
[[0,2],[2,27],[27,33],[33,21],[21,23],[23,21],[21,67],[67,42],[42,22],[22,57],[57,27],[27,0]]]
};
