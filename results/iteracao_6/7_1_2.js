if (typeof results == "undefined")
	window.results = {};
results["LCGDRPP_50_3_10_2_2"] = {
	cost: 5477,
	value: 5477,
	executionTime: 189.955,
	heuristic: "Iterated greedy",
	constructor: "Best edge",
	localSearches: ["Reduce","OneDistinct","Insertion","Exchange"],
	route: [
[[0,22],[22,45],[45,40],[40,14],[14,39],[39,34],[34,32],[32,44],[44,13],[13,41],[41,12],[12,48],[48,9],[9,6],[6,27],[27,33],[33,8],[8,0]],
[[0,20],[20,4],[4,25],[25,46],[46,19],[19,26],[26,29],[29,43],[43,31],[31,11],[11,18],[18,15],[15,20],[20,0]]]
};
