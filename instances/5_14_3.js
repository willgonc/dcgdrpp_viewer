if (typeof instances == "undefined")
	window.instances = {};
instances["A5105_22_gdrpp_3"] = {
	maxDistance: 2809,
	totalArcs: 301,
	totalNodes: 116,
	totalCustomers: 18,
	totalVehicles: 3,
	arcs: {
		"0_19": { "cost": 60, "customers": [3]},
		"0_22": { "cost": 22, "customers": [3]},
		"0_26": { "cost": 50, "customers": []},
		"1_2": { "cost": 59, "customers": [1]},
		"1_21": { "cost": 73, "customers": [1]},
		"2_1": { "cost": 57, "customers": [1]},
		"2_9": { "cost": 136, "customers": [1]},
		"2_11": { "cost": 232, "customers": []},
		"3_6": { "cost": 47, "customers": []},
		"4_3": { "cost": 70, "customers": [2]},
		"4_7": { "cost": 22, "customers": [2]},
		"5_4": { "cost": 17, "customers": [2]},
		"5_8": { "cost": 29, "customers": [2]},
		"6_3": { "cost": 37, "customers": []},
		"6_5": { "cost": 8, "customers": [2]},
		"6_9": { "cost": 38, "customers": []},
		"7_12": { "cost": 31, "customers": []},
		"8_7": { "cost": 28, "customers": []},
		"8_9": { "cost": 20, "customers": []},
		"8_13": { "cost": 33, "customers": [2]},
		"9_2": { "cost": 135, "customers": [1]},
		"9_6": { "cost": 40, "customers": []},
		"9_8": { "cost": 29, "customers": []},
		"9_10": { "cost": 37, "customers": []},
		"9_14": { "cost": 32, "customers": [1]},
		"9_15": { "cost": 26, "customers": []},
		"10_16": { "cost": 31, "customers": [3]},
		"11_12": { "cost": 113, "customers": []},
		"11_29": { "cost": 330, "customers": [4]},
		"12_7": { "cost": 28, "customers": []},
		"12_11": { "cost": 111, "customers": []},
		"12_13": { "cost": 30, "customers": [2]},
		"13_12": { "cost": 15, "customers": [2]},
		"13_14": { "cost": 26, "customers": []},
		"14_9": { "cost": 19, "customers": [1]},
		"14_13": { "cost": 18, "customers": []},
		"14_15": { "cost": 29, "customers": []},
		"14_18": { "cost": 50, "customers": [1]},
		"15_14": { "cost": 17, "customers": []},
		"15_16": { "cost": 22, "customers": [3]},
		"15_19": { "cost": 50, "customers": []},
		"16_15": { "cost": 16, "customers": [3]},
		"17_10": { "cost": 62, "customers": [3]},
		"17_16": { "cost": 35, "customers": []},
		"18_14": { "cost": 34, "customers": [1]},
		"18_19": { "cost": 42, "customers": []},
		"18_24": { "cost": 209, "customers": []},
		"19_0": { "cost": 51, "customers": [3]},
		"19_15": { "cost": 46, "customers": []},
		"19_18": { "cost": 27, "customers": []},
		"19_20": { "cost": 32, "customers": [3]},
		"20_16": { "cost": 35, "customers": [3]},
		"20_17": { "cost": 79, "customers": [3]},
		"20_19": { "cost": 22, "customers": [3]},
		"21_1": { "cost": 67, "customers": [1]},
		"22_0": { "cost": 34, "customers": [3]},
		"23_26": { "cost": 144, "customers": []},
		"24_18": { "cost": 198, "customers": []},
		"24_30": { "cost": 63, "customers": []},
		"25_24": { "cost": 75, "customers": [6]},
		"25_28": { "cost": 8, "customers": []},
		"26_0": { "cost": 68, "customers": []},
		"26_28": { "cost": 82, "customers": [7]},
		"26_33": { "cost": 48, "customers": []},
		"27_23": { "cost": 50, "customers": [5]},
		"27_38": { "cost": 148, "customers": [5]},
		"28_25": { "cost": 14, "customers": []},
		"28_26": { "cost": 79, "customers": [7]},
		"28_32": { "cost": 36, "customers": [7]},
		"29_35": { "cost": 86, "customers": [4]},
		"29_39": { "cost": 105, "customers": [4]},
		"30_24": { "cost": 68, "customers": []},
		"30_31": { "cost": 41, "customers": [6]},
		"30_35": { "cost": 49, "customers": []},
		"31_24": { "cost": 40, "customers": [6]},
		"31_30": { "cost": 52, "customers": [6]},
		"31_32": { "cost": 58, "customers": []},
		"32_28": { "cost": 29, "customers": [7]},
		"32_31": { "cost": 44, "customers": []},
		"33_26": { "cost": 45, "customers": []},
		"33_32": { "cost": 92, "customers": [7]},
		"33_36": { "cost": 80, "customers": []},
		"34_38": { "cost": 99, "customers": [5]},
		"35_30": { "cost": 39, "customers": []},
		"35_36": { "cost": 136, "customers": [4]},
		"35_40": { "cost": 80, "customers": [4]},
		"36_33": { "cost": 84, "customers": []},
		"36_35": { "cost": 144, "customers": [4]},
		"36_37": { "cost": 71, "customers": [4]},
		"36_43": { "cost": 78, "customers": [4]},
		"37_36": { "cost": 78, "customers": [4]},
		"37_38": { "cost": 22, "customers": []},
		"37_44": { "cost": 116, "customers": [4]},
		"37_45": { "cost": 96, "customers": [4]},
		"38_27": { "cost": 143, "customers": [5]},
		"38_34": { "cost": 94, "customers": [5]},
		"38_37": { "cost": 18, "customers": []},
		"39_29": { "cost": 100, "customers": [4]},
		"39_40": { "cost": 63, "customers": []},
		"39_55": { "cost": 218, "customers": [4]},
		"40_35": { "cost": 78, "customers": [4]},
		"40_39": { "cost": 71, "customers": []},
		"40_41": { "cost": 62, "customers": [4]},
		"40_83": { "cost": 232, "customers": []},
		"41_40": { "cost": 80, "customers": [4]},
		"41_42": { "cost": 39, "customers": [4]},
		"41_46": { "cost": 99, "customers": []},
		"42_41": { "cost": 44, "customers": [4]},
		"42_43": { "cost": 34, "customers": []},
		"43_36": { "cost": 80, "customers": [4]},
		"43_42": { "cost": 47, "customers": []},
		"43_47": { "cost": 109, "customers": []},
		"43_48": { "cost": 106, "customers": [4]},
		"44_45": { "cost": 28, "customers": []},
		"45_37": { "cost": 105, "customers": [4]},
		"45_38": { "cost": 131, "customers": []},
		"45_48": { "cost": 68, "customers": []},
		"46_41": { "cost": 106, "customers": []},
		"46_47": { "cost": 64, "customers": [4]},
		"46_84": { "cost": 82, "customers": []},
		"47_43": { "cost": 99, "customers": []},
		"47_46": { "cost": 65, "customers": [4]},
		"47_48": { "cost": 58, "customers": []},
		"47_86": { "cost": 92, "customers": [4]},
		"48_43": { "cost": 103, "customers": [4]},
		"48_45": { "cost": 55, "customers": []},
		"48_47": { "cost": 57, "customers": []},
		"48_87": { "cost": 108, "customers": [4]},
		"49_55": { "cost": 214, "customers": [4]},
		"50_49": { "cost": 43, "customers": [4]},
		"50_51": { "cost": 23, "customers": [4]},
		"50_58": { "cost": 45, "customers": [4]},
		"51_50": { "cost": 23, "customers": [4]},
		"51_52": { "cost": 24, "customers": [4]},
		"52_51": { "cost": 26, "customers": [4]},
		"52_53": { "cost": 46, "customers": [4]},
		"53_52": { "cost": 43, "customers": [4]},
		"53_54": { "cost": 31, "customers": [4]},
		"54_53": { "cost": 39, "customers": [4]},
		"54_61": { "cost": 46, "customers": []},
		"54_62": { "cost": 43, "customers": []},
		"55_39": { "cost": 207, "customers": [4]},
		"55_49": { "cost": 228, "customers": [4]},
		"55_56": { "cost": 59, "customers": [4]},
		"55_62": { "cost": 57, "customers": []},
		"56_55": { "cost": 58, "customers": [4]},
		"56_65": { "cost": 103, "customers": []},
		"56_83": { "cost": 104, "customers": [4]},
		"57_49": { "cost": 101, "customers": [4]},
		"57_62": { "cost": 158, "customers": []},
		"58_59": { "cost": 22, "customers": []},
		"59_51": { "cost": 43, "customers": [4]},
		"59_58": { "cost": 36, "customers": []},
		"59_60": { "cost": 36, "customers": []},
		"60_52": { "cost": 57, "customers": [4]},
		"60_59": { "cost": 31, "customers": []},
		"60_61": { "cost": 56, "customers": [4]},
		"61_53": { "cost": 35, "customers": []},
		"61_54": { "cost": 32, "customers": []},
		"61_60": { "cost": 42, "customers": [4]},
		"62_54": { "cost": 50, "customers": []},
		"62_55": { "cost": 53, "customers": []},
		"62_57": { "cost": 165, "customers": []},
		"62_63": { "cost": 36, "customers": [8]},
		"62_64": { "cost": 58, "customers": [8]},
		"63_62": { "cost": 23, "customers": [8]},
		"64_62": { "cost": 47, "customers": [8]},
		"64_65": { "cost": 35, "customers": []},
		"64_70": { "cost": 34, "customers": [8]},
		"65_56": { "cost": 93, "customers": []},
		"65_64": { "cost": 28, "customers": []},
		"66_67": { "cost": 90, "customers": [9]},
		"66_96": { "cost": 140, "customers": [9]},
		"66_101": { "cost": 67, "customers": [9]},
		"67_66": { "cost": 97, "customers": [9]},
		"67_106": { "cost": 28, "customers": [9]},
		"68_93": { "cost": 108, "customers": []},
		"68_105": { "cost": 173, "customers": [10]},
		"69_70": { "cost": 40, "customers": [8]},
		"69_74": { "cost": 90, "customers": [8]},
		"69_79": { "cost": 79, "customers": [8]},
		"70_64": { "cost": 34, "customers": [8]},
		"70_69": { "cost": 41, "customers": [8]},
		"70_71": { "cost": 42, "customers": [8]},
		"70_75": { "cost": 45, "customers": []},
		"71_65": { "cost": 51, "customers": [8]},
		"71_72": { "cost": 28, "customers": [8]},
		"72_71": { "cost": 34, "customers": [8]},
		"72_76": { "cost": 52, "customers": []},
		"73_74": { "cost": 117, "customers": [8]},
		"74_69": { "cost": 85, "customers": [8]},
		"74_73": { "cost": 126, "customers": [8]},
		"74_78": { "cost": 38, "customers": []},
		"75_70": { "cost": 36, "customers": []},
		"75_76": { "cost": 53, "customers": [11]},
		"75_80": { "cost": 50, "customers": []},
		"76_75": { "cost": 65, "customers": [11]},
		"77_80": { "cost": 52, "customers": [12]},
		"78_81": { "cost": 128, "customers": []},
		"79_69": { "cost": 75, "customers": [8]},
		"79_78": { "cost": 42, "customers": [8]},
		"80_75": { "cost": 44, "customers": []},
		"80_77": { "cost": 67, "customers": [12]},
		"80_79": { "cost": 41, "customers": []},
		"80_81": { "cost": 51, "customers": []},
		"81_80": { "cost": 45, "customers": []},
		"81_82": { "cost": 101, "customers": [4]},
		"82_81": { "cost": 87, "customers": [4]},
		"82_105": { "cost": 397, "customers": []},
		"83_40": { "cost": 226, "customers": []},
		"83_56": { "cost": 114, "customers": [4]},
		"83_84": { "cost": 95, "customers": [4]},
		"83_92": { "cost": 104, "customers": []},
		"83_93": { "cost": 121, "customers": [4]},
		"84_46": { "cost": 93, "customers": []},
		"84_83": { "cost": 101, "customers": [4]},
		"84_85": { "cost": 39, "customers": []},
		"84_92": { "cost": 77, "customers": []},
		"85_84": { "cost": 49, "customers": []},
		"85_86": { "cost": 33, "customers": [4]},
		"85_88": { "cost": 38, "customers": []},
		"86_47": { "cost": 98, "customers": [4]},
		"86_85": { "cost": 39, "customers": [4]},
		"86_87": { "cost": 60, "customers": []},
		"86_89": { "cost": 39, "customers": [4]},
		"87_48": { "cost": 110, "customers": [4]},
		"87_86": { "cost": 60, "customers": []},
		"87_90": { "cost": 43, "customers": [4]},
		"87_91": { "cost": 118, "customers": []},
		"88_85": { "cost": 29, "customers": []},
		"88_89": { "cost": 28, "customers": [4]},
		"88_95": { "cost": 34, "customers": [4]},
		"89_88": { "cost": 36, "customers": [4]},
		"89_90": { "cost": 44, "customers": [4]},
		"89_99": { "cost": 91, "customers": []},
		"90_87": { "cost": 42, "customers": [4]},
		"90_89": { "cost": 43, "customers": [4]},
		"90_91": { "cost": 37, "customers": [4]},
		"90_99": { "cost": 110, "customers": []},
		"91_90": { "cost": 39, "customers": [4]},
		"92_83": { "cost": 112, "customers": []},
		"92_84": { "cost": 92, "customers": []},
		"92_93": { "cost": 98, "customers": [4]},
		"92_94": { "cost": 42, "customers": []},
		"93_68": { "cost": 114, "customers": []},
		"93_82": { "cost": 268, "customers": [4]},
		"93_83": { "cost": 125, "customers": [4]},
		"94_92": { "cost": 34, "customers": []},
		"94_95": { "cost": 28, "customers": []},
		"94_96": { "cost": 43, "customers": [9]},
		"95_88": { "cost": 46, "customers": [4]},
		"95_94": { "cost": 32, "customers": []},
		"95_97": { "cost": 41, "customers": []},
		"95_98": { "cost": 58, "customers": []},
		"96_66": { "cost": 142, "customers": [9]},
		"96_94": { "cost": 53, "customers": [9]},
		"96_97": { "cost": 12, "customers": []},
		"97_95": { "cost": 55, "customers": []},
		"97_96": { "cost": 14, "customers": []},
		"97_98": { "cost": 32, "customers": [13]},
		"98_95": { "cost": 50, "customers": []},
		"99_89": { "cost": 109, "customers": []},
		"99_90": { "cost": 104, "customers": []},
		"99_98": { "cost": 52, "customers": []},
		"99_100": { "cost": 52, "customers": [14]},
		"99_102": { "cost": 158, "customers": [14]},
		"100_99": { "cost": 60, "customers": [14]},
		"101_66": { "cost": 69, "customers": [9]},
		"101_102": { "cost": 38, "customers": []},
		"101_103": { "cost": 38, "customers": []},
		"102_99": { "cost": 177, "customers": [14]},
		"102_104": { "cost": 46, "customers": []},
		"103_101": { "cost": 31, "customers": []},
		"103_104": { "cost": 40, "customers": [15]},
		"104_102": { "cost": 52, "customers": []},
		"104_107": { "cost": 91, "customers": [15]},
		"105_68": { "cost": 174, "customers": [10]},
		"105_82": { "cost": 390, "customers": []},
		"105_106": { "cost": 60, "customers": []},
		"106_67": { "cost": 37, "customers": [9]},
		"106_105": { "cost": 61, "customers": []},
		"106_107": { "cost": 59, "customers": []},
		"107_104": { "cost": 95, "customers": [15]},
		"107_106": { "cost": 50, "customers": []},
		"107_108": { "cost": 69, "customers": [15]},
		"107_109": { "cost": 234, "customers": []},
		"108_107": { "cost": 61, "customers": [15]},
		"109_107": { "cost": 230, "customers": []},
		"109_110": { "cost": 18, "customers": [16]},
		"109_111": { "cost": 83, "customers": []},
		"110_109": { "cost": 16, "customers": [16]},
		"111_109": { "cost": 85, "customers": []},
		"111_112": { "cost": 56, "customers": [17]},
		"111_113": { "cost": 52, "customers": [17]},
		"111_115": { "cost": 96, "customers": []},
		"112_111": { "cost": 48, "customers": [17]},
		"113_111": { "cost": 57, "customers": [17]},
		"113_114": { "cost": 34, "customers": []},
		"114_113": { "cost": 31, "customers": []},
		"115_111": { "cost": 81, "customers": []},
		"115_114": { "cost": 28, "customers": [18]}
	}
};