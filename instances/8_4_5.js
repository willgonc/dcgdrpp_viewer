if (typeof instances == "undefined")
	window.instances = {};
instances["LCGDRPP_75_3_20_2_5"] = {
	maxDistance: 2670,
	totalArcs: 448,
	totalNodes: 75,
	totalCustomers: 150,
	totalVehicles: 5,
	arcs: {
		"0_15": { "cost": 121, "customers": [11,32,37,51,75,142]},
		"0_22": { "cost": 149, "customers": [37,46,51,75,142]},
		"0_35": { "cost": 95, "customers": [5,32,37,75,142]},
		"0_46": { "cost": 182, "customers": [5,15,37,75]},
		"0_51": { "cost": 189, "customers": [142]},
		"0_56": { "cost": 79, "customers": [11,15,37,46,75,142]},
		"1_16": { "cost": 103, "customers": [82,108,111]},
		"1_19": { "cost": 125, "customers": [13,108,111,137,139]},
		"1_24": { "cost": 135, "customers": [49,104,111]},
		"1_58": { "cost": 42, "customers": [36,49,81,111,139]},
		"1_71": { "cost": 221, "customers": [36,104,110,111,137]},
		"1_74": { "cost": 202, "customers": [81,111]},
		"2_27": { "cost": 110, "customers": [1,19,109,131,143,150]},
		"2_29": { "cost": 26, "customers": [1,19,79,86,109,143,150]},
		"2_32": { "cost": 18, "customers": [1,19,30,86,95,143,150]},
		"2_53": { "cost": 183, "customers": [2,19]},
		"2_66": { "cost": 68, "customers": [1,2,19,30,79,135,143,150]},
		"2_67": { "cost": 151, "customers": [19,95,131,143,150]},
		"3_8": { "cost": 114, "customers": [38,62,64]},
		"3_12": { "cost": 104, "customers": [38,45,64,98,123]},
		"3_54": { "cost": 244, "customers": [38,62,70,107]},
		"3_62": { "cost": 156, "customers": [64,88,123]},
		"3_65": { "cost": 239, "customers": [7,38,45,64,107]},
		"3_70": { "cost": 208, "customers": [64,88,98]},
		"4_44": { "cost": 136, "customers": [16,42,53,129,138]},
		"4_52": { "cost": 87, "customers": [16,42,53,91,101,129]},
		"4_57": { "cost": 47, "customers": [16,42,53,91,101,126,129]},
		"4_62": { "cost": 142, "customers": [88,101]},
		"4_68": { "cost": 124, "customers": [16,42,53,129,138]},
		"4_70": { "cost": 83, "customers": [16,53,88,101,126]},
		"5_14": { "cost": 174, "customers": [31,33,65]},
		"5_21": { "cost": 111, "customers": [35,105,112]},
		"5_25": { "cost": 171, "customers": [112,133]},
		"5_43": { "cost": 160, "customers": [35,105]},
		"5_46": { "cost": 171, "customers": [133]},
		"5_69": { "cost": 86, "customers": [33,35]},
		"6_36": { "cost": 170, "customers": [12,145]},
		"6_37": { "cost": 14, "customers": [4,28,113,145]},
		"6_57": { "cost": 128, "customers": [4,113,126]},
		"6_59": { "cost": 152, "customers": [4,12,145]},
		"6_62": { "cost": 150, "customers": [4,28,88]},
		"6_70": { "cost": 120, "customers": [4,28,88,126]},
		"7_27": { "cost": 158, "customers": [23,40,131]},
		"7_41": { "cost": 94, "customers": [14,60,78,94,106,120]},
		"7_42": { "cost": 116, "customers": [14,26,40,63]},
		"7_63": { "cost": 113, "customers": [60,78,94,120]},
		"7_67": { "cost": 117, "customers": [23,40,63,131]},
		"7_73": { "cost": 113, "customers": [14,26,44,60,94]},
		"8_3": { "cost": 117, "customers": [38,62,64]},
		"8_12": { "cost": 165, "customers": [45,64]},
		"8_26": { "cost": 150, "customers": [62,136]},
		"8_54": { "cost": 130, "customers": [62,70,107]},
		"8_60": { "cost": 158, "customers": [7,62,70,136]},
		"8_65": { "cost": 139, "customers": [7,38,45,62,107]},
		"9_14": { "cost": 44, "customers": [3,31,127]},
		"9_23": { "cost": 109, "customers": [3,115,121,127,130]},
		"9_28": { "cost": 35, "customers": [3,57,115,127]},
		"9_34": { "cost": 52, "customers": [3,31,57,74,115,127,130]},
		"9_35": { "cost": 200, "customers": [127]},
		"9_36": { "cost": 207, "customers": [3,115,121]},
		"10_11": { "cost": 104, "customers": [34,52,93,99,103,119,134,140,148]},
		"10_13": { "cost": 95, "customers": [29,90,93,99,102,103,119,146]},
		"10_17": { "cost": 148, "customers": [69,80,93,103,140,148]},
		"10_50": { "cost": 59, "customers": [71,80,93,99,103,134,140]},
		"10_64": { "cost": 167, "customers": [34,90,93,99,102,103,140,144]},
		"10_69": { "cost": 179, "customers": [99,102,144,146]},
		"11_10": { "cost": 100, "customers": [34,52,93,99,103,119,134,140,148]},
		"11_17": { "cost": 57, "customers": [34,52,69,80,103,134,140,148]},
		"11_50": { "cost": 39, "customers": [34,52,71,80,93,103,119,134,148]},
		"11_61": { "cost": 104, "customers": [52,69,71,103,134,148]},
		"11_64": { "cost": 275, "customers": [34,90,103,119,134,148]},
		"12_3": { "cost": 101, "customers": [38,45,64,98,123]},
		"12_8": { "cost": 150, "customers": [45,64]},
		"12_37": { "cost": 189, "customers": [98,123]},
		"12_62": { "cost": 60, "customers": [64,88,98,123]},
		"12_65": { "cost": 250, "customers": [38,45,64]},
		"12_70": { "cost": 97, "customers": [64,88,98,123]},
		"13_10": { "cost": 92, "customers": [29,90,93,99,102,103,119,146]},
		"13_11": { "cost": 157, "customers": [34,90,99,103,119,134]},
		"13_21": { "cost": 142, "customers": [29,67,105,146]},
		"13_43": { "cost": 105, "customers": [29,67,99,105,146]},
		"13_50": { "cost": 108, "customers": [29,67,90,93,99,119,134]},
		"13_69": { "cost": 108, "customers": [29,67,90,99,102,144,146]},
		"14_5": { "cost": 145, "customers": [31,33,65]},
		"14_9": { "cost": 43, "customers": [3,31,127]},
		"14_23": { "cost": 147, "customers": [31,127,130]},
		"14_28": { "cost": 82, "customers": [31,57,127]},
		"14_34": { "cost": 88, "customers": [3,31,57,65,127,130]},
		"14_35": { "cost": 244, "customers": [127]},
		"15_0": { "cost": 140, "customers": [11,32,37,51,75,142]},
		"15_22": { "cost": 13, "customers": [9,11,32,37,46,51,58,92,132,142]},
		"15_35": { "cost": 94, "customers": [5,11,32,37,51,75,92,100]},
		"15_40": { "cost": 115, "customers": [9,51,92,132]},
		"15_47": { "cost": 119, "customers": [9,11,32,51,58,83,92,100]},
		"15_56": { "cost": 55, "customers": [11,15,32,37,46,51,92]},
		"16_1": { "cost": 100, "customers": [82,108,111]},
		"16_19": { "cost": 73, "customers": [13,73,82,108,139]},
		"16_24": { "cost": 94, "customers": [49,84]},
		"16_30": { "cost": 183, "customers": [13,25,82,108]},
		"16_31": { "cost": 190, "customers": [25,73,84,108]},
		"16_58": { "cost": 121, "customers": [49,81,108,111,139]},
		"17_11": { "cost": 56, "customers": [34,52,69,80,103,134,140,148]},
		"17_43": { "cost": 252, "customers": [80,140]},
		"17_50": { "cost": 98, "customers": [69,71,80,93,134,140,148]},
		"17_61": { "cost": 88, "customers": [52,69,71,80,140,148]},
		"17_64": { "cost": 310, "customers": [34,80,140,148]},
		"18_2": { "cost": 115, "customers": [1,19,135,143,150]},
		"18_27": { "cost": 63, "customers": [109,131,135,143,150]},
		"18_32": { "cost": 87, "customers": [30,86,95,135,150]},
		"18_66": { "cost": 59, "customers": [19,30,79,135,150]},
		"18_67": { "cost": 75, "customers": [95,131,135,150]},
		"18_68": { "cost": 121, "customers": [116,122]},
		"19_1": { "cost": 128, "customers": [13,108,111,137,139]},
		"19_16": { "cost": 87, "customers": [13,73,82,108,139]},
		"19_30": { "cost": 128, "customers": [13,25,68,73,108,137]},
		"19_31": { "cost": 161, "customers": [13,25,68,73,76,108,137,139]},
		"19_39": { "cost": 106, "customers": [13,68,82,96,108,110,137]},
		"19_71": { "cost": 112, "customers": [13,36,41,61,68,73,76,96,110,137,139]},
		"20_38": { "cost": 169, "customers": [6,55,59,141]},
		"20_42": { "cost": 162, "customers": [26,66,72,117]},
		"20_45": { "cost": 65, "customers": [44,59,66,117,118]},
		"20_55": { "cost": 200, "customers": [20,55,59,77,117,118,141]},
		"20_66": { "cost": 361, "customers": [66,72]},
		"20_73": { "cost": 84, "customers": [26,44,59,66,77,117,141]},
		"21_5": { "cost": 109, "customers": [35,105,112]},
		"21_13": { "cost": 145, "customers": [29,67,105,146]},
		"21_25": { "cost": 243, "customers": [112]},
		"21_43": { "cost": 65, "customers": [29,35,67,105]},
		"21_50": { "cost": 257, "customers": [67,105]},
		"21_69": { "cost": 85, "customers": [67,105,146]},
		"22_0": { "cost": 128, "customers": [37,46,51,75,142]},
		"22_15": { "cost": 12, "customers": [9,11,32,37,46,51,58,92,132,142]},
		"22_35": { "cost": 100, "customers": [32,46,51,58,75,100,142]},
		"22_40": { "cost": 111, "customers": [9,51,58,132]},
		"22_47": { "cost": 119, "customers": [46,51,58,92,100,132]},
		"22_56": { "cost": 74, "customers": [11,46,51,58,142]},
		"23_9": { "cost": 105, "customers": [3,115,121,127,130]},
		"23_28": { "cost": 79, "customers": [57,121,130]},
		"23_34": { "cost": 60, "customers": [3,31,47,57,97,121,130]},
		"23_36": { "cost": 136, "customers": [47,115,121,130]},
		"23_54": { "cost": 281, "customers": [107,130]},
		"23_59": { "cost": 120, "customers": [47,97,121,130]},
		"24_1": { "cost": 151, "customers": [49,104,111]},
		"24_16": { "cost": 85, "customers": [49,84]},
		"24_28": { "cost": 820, "customers": [147]},
		"24_58": { "cost": 131, "customers": [36,49,84,104,111]},
		"24_61": { "cost": 818, "customers": [89]},
		"24_71": { "cost": 293, "customers": [36,49,76,84,104]},
		"25_35": { "cost": 189, "customers": [5,85,100,133]},
		"25_40": { "cost": 114, "customers": [54,56,133]},
		"25_46": { "cost": 113, "customers": [5,56,83,85,133]},
		"25_47": { "cost": 130, "customers": [83,85,100,133]},
		"25_48": { "cost": 67, "customers": [48,54,56,133]},
		"25_49": { "cost": 170, "customers": [48,54,56]},
		"26_8": { "cost": 148, "customers": [62,136]},
		"26_34": { "cost": 316, "customers": [87,97]},
		"26_54": { "cost": 65, "customers": [62,70,87,107,136]},
		"26_60": { "cost": 88, "customers": [7,70,136]},
		"26_62": { "cost": 230, "customers": []},
		"26_65": { "cost": 11, "customers": [7,107,136]},
		"27_2": { "cost": 121, "customers": [1,19,109,131,143,150]},
		"27_18": { "cost": 62, "customers": [109,131,135,143,150]},
		"27_29": { "cost": 125, "customers": [1,79,86,109,143]},
		"27_32": { "cost": 100, "customers": [30,86,95,109,131,143]},
		"27_66": { "cost": 41, "customers": [19,30,79,109,131,135,143]},
		"27_67": { "cost": 36, "customers": [23,40,95,131,143]},
		"28_9": { "cost": 39, "customers": [3,57,115,127]},
		"28_14": { "cost": 79, "customers": [31,57,127]},
		"28_23": { "cost": 81, "customers": [57,121,130]},
		"28_34": { "cost": 31, "customers": [3,31,57,130]},
		"28_52": { "cost": 506, "customers": []},
		"28_60": { "cost": 364, "customers": [43]},
		"29_2": { "cost": 25, "customers": [1,19,79,86,109,143,150]},
		"29_18": { "cost": 116, "customers": [1,79,86,109,116,122,135,150]},
		"29_32": { "cost": 40, "customers": [1,30,79,86,109]},
		"29_44": { "cost": 179, "customers": [8,79,116,138]},
		"29_66": { "cost": 84, "customers": [1,19,30,79,86,109,135]},
		"29_68": { "cost": 137, "customers": [8,116,122,138]},
		"30_1": { "cost": 240, "customers": [13]},
		"30_19": { "cost": 124, "customers": [13,25,68,73,108,137]},
		"30_31": { "cost": 38, "customers": [13,25,73,76]},
		"30_39": { "cost": 164, "customers": [13,82,110]},
		"30_71": { "cost": 81, "customers": [13,25,61,76,110,137]},
		"30_72": { "cost": 159, "customers": [13,25,61,68]},
		"31_16": { "cost": 188, "customers": [25,73,84,108]},
		"31_24": { "cost": 287, "customers": [49,76,84,104]},
		"31_30": { "cost": 41, "customers": [13,25,73,76]},
		"31_58": { "cost": 301, "customers": [36,49,73,76,84,139]},
		"31_71": { "cost": 108, "customers": [25,36,61,73,76,84,104,137]},
		"31_72": { "cost": 164, "customers": [25,61,68,73,76]},
		"32_2": { "cost": 17, "customers": [1,19,30,86,95,143,150]},
		"32_18": { "cost": 101, "customers": [30,86,95,135,150]},
		"32_27": { "cost": 103, "customers": [30,86,95,109,131,143]},
		"32_29": { "cost": 39, "customers": [1,30,79,86,109]},
		"32_66": { "cost": 53, "customers": [19,30,79,86,95,135]},
		"32_67": { "cost": 128, "customers": [30,95,131]},
		"33_7": { "cost": 166, "customers": [14,60,94,106,120]},
		"33_20": { "cost": 177, "customers": [6,20,59,141]},
		"33_38": { "cost": 27, "customers": [6,20,55,106,124,141]},
		"33_41": { "cost": 148, "customers": [6,20,60,78,94,106,124]},
		"33_55": { "cost": 87, "customers": [6,20,55,59,77,106,114]},
		"33_63": { "cost": 142, "customers": [6,20,78,94,106,114,120,128]},
		"34_5": { "cost": 269, "customers": [31,65]},
		"34_9": { "cost": 53, "customers": [3,31,57,74,115,127,130]},
		"34_23": { "cost": 53, "customers": [3,31,47,57,97,121,130]},
		"34_26": { "cost": 260, "customers": [87,97]},
		"34_28": { "cost": 31, "customers": [3,31,57,130]},
		"34_56": { "cost": 316, "customers": [3,74]},
		"35_0": { "cost": 93, "customers": [5,32,37,75,142]},
		"35_15": { "cost": 91, "customers": [5,11,32,37,51,75,92,100]},
		"35_46": { "cost": 93, "customers": [5,15,32,75,83,85,100,133]},
		"35_47": { "cost": 64, "customers": [5,32,58,83,85,92,100]},
		"35_56": { "cost": 56, "customers": [5,11,15,32,46,75,100]},
		"35_63": { "cost": 313, "customers": [149]},
		"36_6": { "cost": 181, "customers": [12,145]},
		"36_23": { "cost": 146, "customers": [47,115,121,130]},
		"36_28": { "cost": 145, "customers": [57,115,121]},
		"36_29": { "cost": 156, "customers": []},
		"36_34": { "cost": 160, "customers": [3,57,97,115,121,130]},
		"36_59": { "cost": 103, "customers": [12,47,97,121,145]},
		"37_6": { "cost": 13, "customers": [4,28,113,145]},
		"37_12": { "cost": 185, "customers": [98,123]},
		"37_26": { "cost": 183, "customers": []},
		"37_59": { "cost": 161, "customers": [4,145]},
		"37_62": { "cost": 121, "customers": [4,28,88,123]},
		"37_70": { "cost": 109, "customers": [4,88,98,113,126]},
		"38_20": { "cost": 168, "customers": [6,55,59,141]},
		"38_33": { "cost": 30, "customers": [6,20,55,106,124,141]},
		"38_41": { "cost": 175, "customers": [6,55,78,106,124]},
		"38_55": { "cost": 71, "customers": [6,20,55,59,77,114,124,141]},
		"38_63": { "cost": 162, "customers": [6,55,78,114,124]},
		"38_73": { "cost": 200, "customers": [6,55,77,124,141]},
		"39_1": { "cost": 176, "customers": [82,110]},
		"39_16": { "cost": 195, "customers": [82,108]},
		"39_19": { "cost": 108, "customers": [13,68,82,96,108,110,137]},
		"39_30": { "cost": 146, "customers": [13,82,110]},
		"39_49": { "cost": 62, "customers": [41,48,96,110]},
		"39_71": { "cost": 85, "customers": [41,61,110,137]},
		"40_15": { "cost": 117, "customers": [9,51,92,132]},
		"40_22": { "cost": 112, "customers": [9,51,58,132]},
		"40_25": { "cost": 133, "customers": [54,56,133]},
		"40_46": { "cost": 147, "customers": [9,83,133]},
		"40_48": { "cost": 130, "customers": [48,56]},
		"40_49": { "cost": 95, "customers": [48,54]},
		"41_7": { "cost": 87, "customers": [14,60,78,94,106,120]},
		"41_33": { "cost": 131, "customers": [6,20,60,78,94,106,124]},
		"41_38": { "cost": 167, "customers": [6,55,78,106,124]},
		"41_63": { "cost": 33, "customers": [60,78,106,114,120,124,128]},
		"41_66": { "cost": 218, "customers": []},
		"41_73": { "cost": 176, "customers": [14,60,77,106,124]},
		"42_7": { "cost": 115, "customers": [14,26,40,63]},
		"42_20": { "cost": 149, "customers": [26,66,72,117]},
		"42_45": { "cost": 120, "customers": [26,44,66,117]},
		"42_67": { "cost": 137, "customers": [40,63]},
		"42_72": { "cost": 551, "customers": []},
		"42_73": { "cost": 81, "customers": [14,26,44,66]},
		"43_5": { "cost": 170, "customers": [35,105]},
		"43_10": { "cost": 179, "customers": [29,93,99,102,140]},
		"43_13": { "cost": 110, "customers": [29,67,99,105,146]},
		"43_21": { "cost": 59, "customers": [29,35,67,105]},
		"43_50": { "cost": 170, "customers": [29,80,93,105]},
		"43_69": { "cost": 108, "customers": [29,35,102,105,146]},
		"44_4": { "cost": 124, "customers": [16,42,53,129,138]},
		"44_52": { "cost": 50, "customers": [8,16,27,91,129,138]},
		"44_53": { "cost": 21, "customers": [17,27,138]},
		"44_57": { "cost": 123, "customers": [27,53,91,129,138]},
		"44_66": { "cost": 207, "customers": [27,79]},
		"44_68": { "cost": 66, "customers": [17,27,42,116,129,138]},
		"45_7": { "cost": 186, "customers": [14,44]},
		"45_17": { "cost": 1263, "customers": [69]},
		"45_20": { "cost": 68, "customers": [44,59,66,117,118]},
		"45_42": { "cost": 136, "customers": [26,44,66,117]},
		"45_53": { "cost": 304, "customers": [18]},
		"45_73": { "cost": 71, "customers": [14,18,26,44,77,117,118]},
		"46_15": { "cost": 153, "customers": [5,9,11,15,32,37,83,92]},
		"46_25": { "cost": 125, "customers": [5,56,83,85,133]},
		"46_35": { "cost": 80, "customers": [5,15,32,75,83,85,100,133]},
		"46_47": { "cost": 34, "customers": [5,15,83,92,100,133]},
		"46_48": { "cost": 204, "customers": [56,133]},
		"46_56": { "cost": 131, "customers": [5,11,15,83]},
		"47_15": { "cost": 119, "customers": [9,11,32,51,58,83,92,100]},
		"47_22": { "cost": 115, "customers": [46,51,58,92,100,132]},
		"47_35": { "cost": 66, "customers": [5,32,58,83,85,92,100]},
		"47_40": { "cost": 127, "customers": [9,58,83,92,132]},
		"47_46": { "cost": 30, "customers": [5,15,83,92,100,133]},
		"47_56": { "cost": 102, "customers": [11,15,46,58,83,92,100]},
		"48_25": { "cost": 72, "customers": [48,54,56,133]},
		"48_39": { "cost": 182, "customers": [48,110]},
		"48_40": { "cost": 146, "customers": [48,56]},
		"48_49": { "cost": 139, "customers": [41,48,54,56]},
		"48_71": { "cost": 249, "customers": [41,48,110]},
		"48_74": { "cost": 222, "customers": []},
		"49_19": { "cost": 155, "customers": [41,68,96,137]},
		"49_39": { "cost": 59, "customers": [41,48,96,110]},
		"49_40": { "cost": 91, "customers": [48,54]},
		"49_48": { "cost": 137, "customers": [41,48,54,56]},
		"49_71": { "cost": 120, "customers": [41,48,61,96,110,137]},
		"49_72": { "cost": 182, "customers": [41,61,68,96]},
		"50_9": { "cost": 360, "customers": [10,71]},
		"50_10": { "cost": 63, "customers": [71,80,93,99,103,134,140]},
		"50_11": { "cost": 41, "customers": [34,52,71,80,93,103,119,134,148]},
		"50_13": { "cost": 96, "customers": [29,67,90,93,99,119,134]},
		"50_17": { "cost": 91, "customers": [69,71,80,93,134,140,148]},
		"50_61": { "cost": 137, "customers": [10,52,69,71,80,93,134]},
		"51_22": { "cost": 186, "customers": [46,142]},
		"51_33": { "cost": 235, "customers": [106,128]},
		"51_41": { "cost": 141, "customers": [78,106,128]},
		"51_56": { "cost": 199, "customers": [46]},
		"51_63": { "cost": 114, "customers": [78,128]},
		"51_72": { "cost": 175, "customers": [22]},
		"52_4": { "cost": 80, "customers": [16,42,53,91,101,129]},
		"52_29": { "cost": 208, "customers": [8,116]},
		"52_44": { "cost": 47, "customers": [8,16,27,91,129,138]},
		"52_45": { "cost": 428, "customers": [18,44]},
		"52_53": { "cost": 69, "customers": [17,18,24,27,91]},
		"52_73": { "cost": 327, "customers": [18,44]},
		"53_44": { "cost": 21, "customers": [17,27,138]},
		"53_52": { "cost": 72, "customers": [17,18,24,27,91]},
		"53_54": { "cost": 507, "customers": [24]},
		"53_57": { "cost": 138, "customers": [17,27,91,126]},
		"53_68": { "cost": 81, "customers": [17,27,138]},
		"53_70": { "cost": 217, "customers": [24,126]},
		"54_26": { "cost": 65, "customers": [62,70,87,107,136]},
		"54_34": { "cost": 364, "customers": [87,130]},
		"54_52": { "cost": 447, "customers": [24]},
		"54_60": { "cost": 29, "customers": [7,62,70,107,136]},
		"54_65": { "cost": 70, "customers": [7,38,62,70,107]},
		"54_70": { "cost": 279, "customers": [24,70]},
		"55_20": { "cost": 223, "customers": [20,55,59,77,117,118,141]},
		"55_33": { "cost": 88, "customers": [6,20,55,59,77,106,114]},
		"55_38": { "cost": 71, "customers": [6,20,55,59,77,114,124,141]},
		"55_41": { "cost": 204, "customers": [20,55,77,78,106,114,124]},
		"55_45": { "cost": 281, "customers": [44,59,77,117,118]},
		"55_63": { "cost": 213, "customers": [20,55,78,114]},
		"56_9": { "cost": 268, "customers": [3,74]},
		"56_15": { "cost": 55, "customers": [11,15,32,37,46,51,92]},
		"56_22": { "cost": 68, "customers": [11,46,51,58,142]},
		"56_35": { "cost": 56, "customers": [5,11,15,32,46,75,100]},
		"56_49": { "cost": 232, "customers": [39]},
		"56_51": { "cost": 207, "customers": [46]},
		"57_4": { "cost": 39, "customers": [16,42,53,91,101,126,129]},
		"57_6": { "cost": 118, "customers": [4,113,126]},
		"57_37": { "cost": 113, "customers": [4,113,126]},
		"57_44": { "cost": 128, "customers": [27,53,91,129,138]},
		"57_52": { "cost": 92, "customers": [16,53,91,126]},
		"57_70": { "cost": 95, "customers": [53,91,101,113,126]},
		"58_1": { "cost": 40, "customers": [36,49,81,111,139]},
		"58_16": { "cost": 108, "customers": [49,81,108,111,139]},
		"58_19": { "cost": 163, "customers": [36,73,108,111,137,139]},
		"58_21": { "cost": 475, "customers": [21,81]},
		"58_24": { "cost": 122, "customers": [36,49,84,104,111]},
		"58_74": { "cost": 226, "customers": [21,81,111]},
		"59_23": { "cost": 121, "customers": [47,97,121,130]},
		"59_26": { "cost": 120, "customers": [87,97]},
		"59_34": { "cost": 166, "customers": [47,87,97,130]},
		"59_36": { "cost": 100, "customers": [12,47,97,121,145]},
		"59_37": { "cost": 177, "customers": [4,145]},
		"59_65": { "cost": 138, "customers": [47]},
		"60_3": { "cost": 249, "customers": [7,38,70]},
		"60_11": { "cost": 586, "customers": []},
		"60_26": { "cost": 81, "customers": [7,70,136]},
		"60_28": { "cost": 344, "customers": [43]},
		"60_54": { "cost": 28, "customers": [7,62,70,107,136]},
		"60_65": { "cost": 91, "customers": [7,38,70,107,136]},
		"61_9": { "cost": 514, "customers": [10,71]},
		"61_10": { "cost": 189, "customers": [52,69,71,93,103,140]},
		"61_11": { "cost": 107, "customers": [52,69,71,103,134,148]},
		"61_17": { "cost": 85, "customers": [52,69,71,80,140,148]},
		"61_45": { "cost": 1132, "customers": [69]},
		"61_50": { "cost": 131, "customers": [10,52,69,71,80,93,134]},
		"62_3": { "cost": 159, "customers": [64,88,123]},
		"62_4": { "cost": 144, "customers": [88,101]},
		"62_6": { "cost": 129, "customers": [4,28,88]},
		"62_12": { "cost": 57, "customers": [64,88,98,123]},
		"62_37": { "cost": 135, "customers": [4,28,88,123]},
		"62_70": { "cost": 59, "customers": [28,88,98,101,123]},
		"63_7": { "cost": 119, "customers": [60,78,94,120]},
		"63_33": { "cost": 137, "customers": [6,20,78,94,106,114,120,128]},
		"63_38": { "cost": 173, "customers": [6,55,78,114,124]},
		"63_41": { "cost": 38, "customers": [60,78,106,114,120,124,128]},
		"63_51": { "cost": 113, "customers": [78,128]},
		"63_55": { "cost": 204, "customers": [20,55,78,114]},
		"64_10": { "cost": 174, "customers": [34,90,93,99,102,103,140,144]},
		"64_11": { "cost": 259, "customers": [34,90,103,119,134,148]},
		"64_13": { "cost": 260, "customers": [34,90,99,119,144,146]},
		"64_14": { "cost": 249, "customers": [33,125,144]},
		"64_50": { "cost": 215, "customers": [34,80,90,93,134]},
		"64_69": { "cost": 274, "customers": [33,90,102,125,144,146]},
		"65_3": { "cost": 240, "customers": [7,38,45,64,107]},
		"65_8": { "cost": 163, "customers": [7,38,45,62,107]},
		"65_23": { "cost": 231, "customers": [47,107]},
		"65_26": { "cost": 12, "customers": [7,107,136]},
		"65_54": { "cost": 73, "customers": [7,38,62,70,107]},
		"65_60": { "cost": 93, "customers": [7,38,70,107,136]},
		"66_2": { "cost": 72, "customers": [1,2,19,30,79,135,143,150]},
		"66_18": { "cost": 69, "customers": [19,30,79,135,150]},
		"66_27": { "cost": 46, "customers": [19,30,79,109,131,135,143]},
		"66_32": { "cost": 55, "customers": [19,30,79,86,95,135]},
		"66_42": { "cost": 225, "customers": [63,66,72]},
		"66_53": { "cost": 173, "customers": [2,19,27]},
		"67_7": { "cost": 107, "customers": [23,40,63,131]},
		"67_18": { "cost": 87, "customers": [95,131,135,150]},
		"67_27": { "cost": 36, "customers": [23,40,95,131,143]},
		"67_32": { "cost": 142, "customers": [30,95,131]},
		"67_42": { "cost": 159, "customers": [40,63]},
		"67_66": { "cost": 86, "customers": [19,30,63,95,131,135]},
		"68_4": { "cost": 131, "customers": [16,42,53,129,138]},
		"68_18": { "cost": 115, "customers": [116,122]},
		"68_44": { "cost": 68, "customers": [17,27,42,116,129,138]},
		"68_52": { "cost": 80, "customers": [8,16,17,42,91,116,138]},
		"68_53": { "cost": 78, "customers": [17,27,138]},
		"68_57": { "cost": 85, "customers": [17,42,53,91,138]},
		"69_5": { "cost": 96, "customers": [33,35]},
		"69_10": { "cost": 184, "customers": [99,102,144,146]},
		"69_13": { "cost": 103, "customers": [29,67,90,99,102,144,146]},
		"69_14": { "cost": 178, "customers": [33,125,144]},
		"69_21": { "cost": 93, "customers": [67,105,146]},
		"69_43": { "cost": 104, "customers": [29,35,102,105,146]},
		"70_4": { "cost": 72, "customers": [16,53,88,101,126]},
		"70_12": { "cost": 109, "customers": [64,88,98,123]},
		"70_52": { "cost": 171, "customers": [16,91,101,126]},
		"70_57": { "cost": 84, "customers": [53,91,101,113,126]},
		"70_60": { "cost": 329, "customers": [70]},
		"70_62": { "cost": 59, "customers": [28,88,98,101,123]},
		"71_19": { "cost": 114, "customers": [13,36,41,61,68,73,76,96,110,137,139]},
		"71_30": { "cost": 74, "customers": [13,25,61,76,110,137]},
		"71_31": { "cost": 112, "customers": [25,36,61,73,76,84,104,137]},
		"71_39": { "cost": 79, "customers": [41,61,110,137]},
		"71_49": { "cost": 106, "customers": [41,48,61,96,110,137]},
		"71_58": { "cost": 248, "customers": [36,49,76,104,111,137,139]},
		"72_19": { "cost": 231, "customers": [13,61,68,73,96,137]},
		"72_30": { "cost": 151, "customers": [13,25,61,68]},
		"72_31": { "cost": 166, "customers": [25,61,68,73,76]},
		"72_39": { "cost": 185, "customers": [61,68,110]},
		"72_51": { "cost": 154, "customers": [22]},
		"72_71": { "cost": 135, "customers": [41,61,68,76,110,137]},
		"73_20": { "cost": 73, "customers": [26,44,59,66,77,117,141]},
		"73_24": { "cost": 828, "customers": [50]},
		"73_33": { "cost": 172, "customers": [6,14,20,77,94,106]},
		"73_42": { "cost": 83, "customers": [14,26,44,66]},
		"73_45": { "cost": 76, "customers": [14,18,26,44,77,117,118]},
		"73_55": { "cost": 263, "customers": [20,44,55,59,77,118]},
		"74_1": { "cost": 211, "customers": [81,111]},
		"74_16": { "cost": 323, "customers": [81]},
		"74_21": { "cost": 310, "customers": [21,81]},
		"74_48": { "cost": 217, "customers": []},
		"74_58": { "cost": 216, "customers": [21,81,111]},
		"74_69": { "cost": 392, "customers": []}
	}
};