if (typeof instances == "undefined")
	window.instances = {};
instances["M3201_gdrpp_157_2"] = {
	maxDistance: 5564,
	totalArcs: 539,
	totalNodes: 196,
	totalCustomers: 47,
	totalVehicles: 2,
	arcs: {
		"0_147": { "cost": 111, "customers": [34]},
		"0_157": { "cost": 66, "customers": []},
		"0_176": { "cost": 120, "customers": []},
		"1_2": { "cost": 52, "customers": []},
		"1_24": { "cost": 49, "customers": []},
		"2_21": { "cost": 37, "customers": [2]},
		"2_23": { "cost": 66, "customers": []},
		"3_2": { "cost": 69, "customers": [2]},
		"4_3": { "cost": 65, "customers": [2]},
		"4_5": { "cost": 59, "customers": []},
		"5_192": { "cost": 62, "customers": []},
		"6_7": { "cost": 110, "customers": [4]},
		"6_14": { "cost": 55, "customers": [4]},
		"6_192": { "cost": 73, "customers": []},
		"7_193": { "cost": 80, "customers": []},
		"8_9": { "cost": 26, "customers": []},
		"8_194": { "cost": 70, "customers": []},
		"9_8": { "cost": 22, "customers": []},
		"9_44": { "cost": 33, "customers": [6]},
		"9_195": { "cost": 46, "customers": []},
		"10_9": { "cost": 55, "customers": []},
		"10_42": { "cost": 27, "customers": [7]},
		"11_10": { "cost": 37, "customers": [7]},
		"11_40": { "cost": 60, "customers": []},
		"11_194": { "cost": 43, "customers": []},
		"12_11": { "cost": 42, "customers": [7]},
		"12_13": { "cost": 37, "customers": [7]},
		"12_39": { "cost": 57, "customers": [7]},
		"13_12": { "cost": 31, "customers": [7]},
		"13_14": { "cost": 61, "customers": []},
		"13_38": { "cost": 72, "customers": []},
		"13_193": { "cost": 39, "customers": []},
		"14_6": { "cost": 60, "customers": [4]},
		"14_7": { "cost": 86, "customers": []},
		"14_13": { "cost": 63, "customers": []},
		"14_15": { "cost": 101, "customers": [4]},
		"15_14": { "cost": 100, "customers": [4]},
		"15_16": { "cost": 45, "customers": []},
		"15_190": { "cost": 33, "customers": [4]},
		"16_15": { "cost": 35, "customers": []},
		"16_17": { "cost": 50, "customers": [3]},
		"16_192": { "cost": 80, "customers": [3]},
		"17_5": { "cost": 95, "customers": [3]},
		"17_16": { "cost": 50, "customers": [3]},
		"17_18": { "cost": 44, "customers": [3]},
		"18_4": { "cost": 127, "customers": []},
		"18_17": { "cost": 41, "customers": [3]},
		"18_20": { "cost": 53, "customers": [3]},
		"18_191": { "cost": 35, "customers": [3]},
		"19_3": { "cost": 55, "customers": []},
		"19_20": { "cost": 65, "customers": [3]},
		"20_18": { "cost": 49, "customers": [3]},
		"20_19": { "cost": 63, "customers": [3]},
		"20_22": { "cost": 63, "customers": []},
		"20_33": { "cost": 75, "customers": [3]},
		"21_19": { "cost": 35, "customers": []},
		"21_22": { "cost": 64, "customers": [2]},
		"22_20": { "cost": 65, "customers": []},
		"22_21": { "cost": 65, "customers": [2]},
		"22_23": { "cost": 49, "customers": []},
		"22_32": { "cost": 74, "customers": []},
		"23_2": { "cost": 61, "customers": []},
		"23_22": { "cost": 51, "customers": []},
		"23_24": { "cost": 47, "customers": [8]},
		"23_32": { "cost": 57, "customers": [8]},
		"24_1": { "cost": 49, "customers": []},
		"24_23": { "cost": 47, "customers": [8]},
		"24_30": { "cost": 126, "customers": []},
		"25_24": { "cost": 43, "customers": []},
		"25_26": { "cost": 52, "customers": [1]},
		"25_156": { "cost": 53, "customers": [1]},
		"26_25": { "cost": 49, "customers": [1]},
		"27_26": { "cost": 137, "customers": []},
		"27_29": { "cost": 105, "customers": [1]},
		"28_27": { "cost": 51, "customers": []},
		"28_29": { "cost": 95, "customers": [1]},
		"28_61": { "cost": 49, "customers": [1]},
		"28_70": { "cost": 228, "customers": []},
		"29_25": { "cost": 150, "customers": [1]},
		"29_27": { "cost": 102, "customers": [1]},
		"29_28": { "cost": 97, "customers": [1]},
		"29_30": { "cost": 36, "customers": [1]},
		"29_59": { "cost": 70, "customers": []},
		"29_60": { "cost": 49, "customers": []},
		"30_24": { "cost": 125, "customers": []},
		"30_29": { "cost": 38, "customers": [1]},
		"30_31": { "cost": 44, "customers": [1]},
		"31_30": { "cost": 45, "customers": [1]},
		"31_32": { "cost": 40, "customers": []},
		"31_58": { "cost": 39, "customers": []},
		"32_22": { "cost": 70, "customers": []},
		"32_23": { "cost": 59, "customers": [8]},
		"32_31": { "cost": 31, "customers": []},
		"33_20": { "cost": 77, "customers": [3]},
		"33_35": { "cost": 76, "customers": [3]},
		"33_57": { "cost": 49, "customers": [3]},
		"33_191": { "cost": 39, "customers": [3]},
		"34_16": { "cost": 135, "customers": [3]},
		"34_17": { "cost": 62, "customers": [3]},
		"34_191": { "cost": 39, "customers": [3]},
		"35_33": { "cost": 78, "customers": [3]},
		"36_37": { "cost": 41, "customers": []},
		"37_38": { "cost": 77, "customers": [9]},
		"37_49": { "cost": 112, "customers": [9]},
		"37_52": { "cost": 70, "customers": []},
		"38_13": { "cost": 70, "customers": []},
		"38_37": { "cost": 79, "customers": [9]},
		"38_39": { "cost": 49, "customers": []},
		"39_12": { "cost": 60, "customers": [7]},
		"39_40": { "cost": 44, "customers": [7]},
		"40_11": { "cost": 69, "customers": []},
		"40_39": { "cost": 41, "customers": [7]},
		"40_41": { "cost": 21, "customers": []},
		"40_49": { "cost": 70, "customers": []},
		"41_40": { "cost": 25, "customers": []},
		"41_42": { "cost": 26, "customers": []},
		"41_43": { "cost": 28, "customers": [10]},
		"42_41": { "cost": 35, "customers": []},
		"42_44": { "cost": 34, "customers": []},
		"43_41": { "cost": 32, "customers": [10]},
		"43_44": { "cost": 31, "customers": []},
		"43_45": { "cost": 41, "customers": [10]},
		"43_48": { "cost": 78, "customers": [10]},
		"44_9": { "cost": 28, "customers": [6]},
		"44_43": { "cost": 28, "customers": []},
		"45_47": { "cost": 89, "customers": []},
		"46_47": { "cost": 72, "customers": [11]},
		"46_86": { "cost": 95, "customers": []},
		"47_46": { "cost": 67, "customers": [11]},
		"47_48": { "cost": 42, "customers": []},
		"48_43": { "cost": 82, "customers": [10]},
		"48_47": { "cost": 45, "customers": []},
		"48_49": { "cost": 54, "customers": []},
		"49_40": { "cost": 65, "customers": []},
		"49_48": { "cost": 52, "customers": []},
		"49_51": { "cost": 48, "customers": [9]},
		"50_46": { "cost": 111, "customers": []},
		"50_51": { "cost": 36, "customers": [9]},
		"50_83": { "cost": 76, "customers": []},
		"51_49": { "cost": 48, "customers": [9]},
		"51_50": { "cost": 41, "customers": [9]},
		"51_54": { "cost": 40, "customers": []},
		"52_37": { "cost": 63, "customers": []},
		"52_51": { "cost": 102, "customers": [9]},
		"52_53": { "cost": 35, "customers": [9]},
		"52_55": { "cost": 48, "customers": []},
		"53_36": { "cost": 64, "customers": [9]},
		"53_52": { "cost": 32, "customers": [9]},
		"53_56": { "cost": 42, "customers": []},
		"54_51": { "cost": 49, "customers": []},
		"54_55": { "cost": 80, "customers": [12]},
		"54_82": { "cost": 17, "customers": [12]},
		"55_52": { "cost": 40, "customers": []},
		"55_54": { "cost": 76, "customers": [12]},
		"55_56": { "cost": 35, "customers": [12]},
		"55_80": { "cost": 89, "customers": []},
		"56_55": { "cost": 45, "customers": [12]},
		"56_57": { "cost": 120, "customers": []},
		"57_33": { "cost": 40, "customers": [3]},
		"57_56": { "cost": 117, "customers": []},
		"57_67": { "cost": 51, "customers": []},
		"58_31": { "cost": 41, "customers": []},
		"58_59": { "cost": 40, "customers": [13]},
		"59_29": { "cost": 63, "customers": []},
		"59_58": { "cost": 39, "customers": [13]},
		"59_65": { "cost": 59, "customers": []},
		"59_66": { "cost": 65, "customers": []},
		"60_29": { "cost": 50, "customers": []},
		"60_61": { "cost": 105, "customers": [1]},
		"60_64": { "cost": 56, "customers": [1]},
		"61_62": { "cost": 64, "customers": [1]},
		"62_61": { "cost": 56, "customers": [1]},
		"62_63": { "cost": 48, "customers": []},
		"62_73": { "cost": 123, "customers": []},
		"63_64": { "cost": 62, "customers": []},
		"63_188": { "cost": 58, "customers": [14]},
		"64_63": { "cost": 60, "customers": []},
		"64_65": { "cost": 55, "customers": []},
		"65_59": { "cost": 51, "customers": []},
		"65_64": { "cost": 52, "customers": []},
		"65_66": { "cost": 53, "customers": []},
		"65_75": { "cost": 117, "customers": [15]},
		"66_59": { "cost": 68, "customers": []},
		"66_65": { "cost": 54, "customers": []},
		"66_67": { "cost": 113, "customers": [16]},
		"66_77": { "cost": 145, "customers": [16]},
		"67_57": { "cost": 53, "customers": []},
		"67_66": { "cost": 113, "customers": [16]},
		"67_68": { "cost": 50, "customers": []},
		"68_67": { "cost": 54, "customers": []},
		"68_78": { "cost": 67, "customers": []},
		"69_56": { "cost": 62, "customers": []},
		"69_68": { "cost": 88, "customers": [17]},
		"70_28": { "cost": 226, "customers": []},
		"70_71": { "cost": 40, "customers": []},
		"70_101": { "cost": 94, "customers": [18]},
		"71_70": { "cost": 44, "customers": []},
		"72_71": { "cost": 44, "customers": [19]},
		"72_73": { "cost": 41, "customers": []},
		"72_103": { "cost": 88, "customers": []},
		"73_62": { "cost": 123, "customers": []},
		"73_72": { "cost": 39, "customers": []},
		"73_74": { "cost": 46, "customers": []},
		"73_99": { "cost": 50, "customers": [20]},
		"74_73": { "cost": 51, "customers": []},
		"74_75": { "cost": 87, "customers": []},
		"74_188": { "cost": 58, "customers": [14]},
		"75_65": { "cost": 118, "customers": [15]},
		"75_74": { "cost": 95, "customers": []},
		"75_76": { "cost": 78, "customers": [15]},
		"75_98": { "cost": 69, "customers": []},
		"76_75": { "cost": 82, "customers": [15]},
		"76_77": { "cost": 87, "customers": []},
		"76_97": { "cost": 75, "customers": [15]},
		"77_66": { "cost": 141, "customers": [16]},
		"77_76": { "cost": 85, "customers": []},
		"77_78": { "cost": 40, "customers": []},
		"77_96": { "cost": 121, "customers": []},
		"78_68": { "cost": 67, "customers": []},
		"78_77": { "cost": 31, "customers": []},
		"78_79": { "cost": 90, "customers": []},
		"78_109": { "cost": 199, "customers": [21]},
		"79_69": { "cost": 55, "customers": []},
		"79_78": { "cost": 92, "customers": []},
		"79_80": { "cost": 44, "customers": [22]},
		"80_55": { "cost": 85, "customers": []},
		"80_79": { "cost": 41, "customers": [22]},
		"80_81": { "cost": 48, "customers": []},
		"80_189": { "cost": 36, "customers": [22]},
		"81_80": { "cost": 51, "customers": []},
		"81_82": { "cost": 42, "customers": [12]},
		"81_90": { "cost": 161, "customers": [12]},
		"81_189": { "cost": 74, "customers": []},
		"82_54": { "cost": 15, "customers": [12]},
		"82_81": { "cost": 42, "customers": [12]},
		"82_83": { "cost": 42, "customers": []},
		"83_82": { "cost": 44, "customers": []},
		"83_84": { "cost": 41, "customers": [23]},
		"84_83": { "cost": 45, "customers": [23]},
		"84_85": { "cost": 85, "customers": [23]},
		"84_86": { "cost": 115, "customers": []},
		"85_84": { "cost": 83, "customers": [23]},
		"86_46": { "cost": 95, "customers": []},
		"86_84": { "cost": 118, "customers": []},
		"86_89": { "cost": 39, "customers": [24]},
		"87_88": { "cost": 197, "customers": [24]},
		"87_117": { "cost": 138, "customers": []},
		"88_87": { "cost": 203, "customers": [24]},
		"88_89": { "cost": 50, "customers": [24]},
		"88_116": { "cost": 151, "customers": []},
		"89_86": { "cost": 36, "customers": [24]},
		"89_88": { "cost": 49, "customers": [24]},
		"89_90": { "cost": 47, "customers": []},
		"90_81": { "cost": 163, "customers": [12]},
		"90_89": { "cost": 52, "customers": []},
		"90_92": { "cost": 63, "customers": [12]},
		"91_81": { "cost": 101, "customers": [12]},
		"91_90": { "cost": 140, "customers": []},
		"92_114": { "cost": 142, "customers": [12]},
		"93_92": { "cost": 105, "customers": []},
		"93_94": { "cost": 86, "customers": []},
		"93_113": { "cost": 116, "customers": [25]},
		"94_91": { "cost": 53, "customers": [12]},
		"94_93": { "cost": 87, "customers": []},
		"94_95": { "cost": 50, "customers": []},
		"95_94": { "cost": 46, "customers": []},
		"95_109": { "cost": 120, "customers": []},
		"95_189": { "cost": 84, "customers": [22]},
		"96_77": { "cost": 121, "customers": []},
		"96_97": { "cost": 110, "customers": [15]},
		"96_108": { "cost": 43, "customers": [15]},
		"97_76": { "cost": 75, "customers": [15]},
		"97_96": { "cost": 108, "customers": [15]},
		"97_187": { "cost": 36, "customers": []},
		"98_75": { "cost": 61, "customers": []},
		"98_99": { "cost": 128, "customers": [20]},
		"98_105": { "cost": 37, "customers": []},
		"99_73": { "cost": 43, "customers": [20]},
		"99_98": { "cost": 126, "customers": [20]},
		"99_104": { "cost": 74, "customers": [20]},
		"100_101": { "cost": 292, "customers": [18]},
		"101_70": { "cost": 90, "customers": [18]},
		"101_128": { "cost": 87, "customers": [18]},
		"102_71": { "cost": 81, "customers": [19]},
		"102_101": { "cost": 52, "customers": []},
		"102_103": { "cost": 40, "customers": []},
		"103_72": { "cost": 91, "customers": []},
		"103_102": { "cost": 42, "customers": []},
		"103_104": { "cost": 80, "customers": []},
		"103_128": { "cost": 119, "customers": [18]},
		"104_99": { "cost": 69, "customers": [20]},
		"104_103": { "cost": 85, "customers": []},
		"104_105": { "cost": 143, "customers": []},
		"104_126": { "cost": 144, "customers": [20]},
		"104_127": { "cost": 69, "customers": []},
		"105_98": { "cost": 39, "customers": []},
		"105_104": { "cost": 149, "customers": []},
		"105_126": { "cost": 149, "customers": []},
		"105_187": { "cost": 37, "customers": [26]},
		"106_107": { "cost": 96, "customers": []},
		"106_125": { "cost": 134, "customers": []},
		"106_187": { "cost": 38, "customers": [26]},
		"107_106": { "cost": 96, "customers": []},
		"107_109": { "cost": 65, "customers": [21]},
		"107_124": { "cost": 101, "customers": []},
		"108_96": { "cost": 48, "customers": [15]},
		"109_78": { "cost": 199, "customers": [21]},
		"109_95": { "cost": 123, "customers": []},
		"109_107": { "cost": 57, "customers": [21]},
		"109_110": { "cost": 96, "customers": []},
		"109_123": { "cost": 72, "customers": []},
		"110_109": { "cost": 96, "customers": []},
		"110_111": { "cost": 58, "customers": []},
		"110_112": { "cost": 63, "customers": [27]},
		"110_123": { "cost": 96, "customers": []},
		"111_94": { "cost": 130, "customers": [12]},
		"111_112": { "cost": 48, "customers": []},
		"112_110": { "cost": 70, "customers": [27]},
		"112_111": { "cost": 47, "customers": []},
		"112_113": { "cost": 73, "customers": []},
		"112_119": { "cost": 95, "customers": []},
		"112_121": { "cost": 126, "customers": [27]},
		"113_93": { "cost": 114, "customers": [25]},
		"113_112": { "cost": 73, "customers": []},
		"113_114": { "cost": 44, "customers": []},
		"114_113": { "cost": 40, "customers": []},
		"114_115": { "cost": 85, "customers": []},
		"114_118": { "cost": 155, "customers": [12]},
		"115_114": { "cost": 95, "customers": []},
		"115_116": { "cost": 45, "customers": [28]},
		"115_118": { "cost": 74, "customers": []},
		"116_88": { "cost": 147, "customers": []},
		"116_115": { "cost": 35, "customers": [28]},
		"116_117": { "cost": 106, "customers": [28]},
		"117_116": { "cost": 105, "customers": [28]},
		"118_114": { "cost": 150, "customers": [12]},
		"119_112": { "cost": 99, "customers": []},
		"119_163": { "cost": 55, "customers": [29]},
		"120_119": { "cost": 139, "customers": []},
		"120_121": { "cost": 54, "customers": [27]},
		"120_162": { "cost": 53, "customers": []},
		"121_112": { "cost": 130, "customers": [27]},
		"121_120": { "cost": 49, "customers": [27]},
		"121_122": { "cost": 24, "customers": []},
		"121_152": { "cost": 83, "customers": []},
		"122_121": { "cost": 25, "customers": []},
		"122_123": { "cost": 50, "customers": [30]},
		"122_144": { "cost": 97, "customers": []},
		"123_109": { "cost": 76, "customers": []},
		"123_122": { "cost": 46, "customers": [30]},
		"123_185": { "cost": 53, "customers": [30]},
		"124_125": { "cost": 80, "customers": []},
		"124_144": { "cost": 48, "customers": [31]},
		"124_185": { "cost": 49, "customers": []},
		"125_106": { "cost": 131, "customers": []},
		"125_124": { "cost": 75, "customers": []},
		"125_126": { "cost": 78, "customers": [20]},
		"125_143": { "cost": 58, "customers": []},
		"126_104": { "cost": 135, "customers": [20]},
		"126_105": { "cost": 155, "customers": []},
		"126_125": { "cost": 76, "customers": [20]},
		"126_142": { "cost": 67, "customers": []},
		"127_104": { "cost": 61, "customers": []},
		"127_134": { "cost": 126, "customers": [18]},
		"127_136": { "cost": 65, "customers": [18]},
		"128_101": { "cost": 93, "customers": [18]},
		"128_103": { "cost": 115, "customers": [18]},
		"128_134": { "cost": 92, "customers": [18]},
		"128_186": { "cost": 75, "customers": []},
		"129_100": { "cost": 173, "customers": []},
		"129_130": { "cost": 220, "customers": [32]},
		"129_186": { "cost": 125, "customers": []},
		"130_129": { "cost": 212, "customers": [32]},
		"131_129": { "cost": 109, "customers": []},
		"131_132": { "cost": 106, "customers": [33]},
		"132_131": { "cost": 101, "customers": [33]},
		"132_134": { "cost": 64, "customers": []},
		"132_186": { "cost": 83, "customers": [33]},
		"133_132": { "cost": 70, "customers": [33]},
		"133_135": { "cost": 82, "customers": [33]},
		"134_127": { "cost": 127, "customers": [18]},
		"134_128": { "cost": 89, "customers": [18]},
		"134_132": { "cost": 65, "customers": []},
		"134_135": { "cost": 35, "customers": []},
		"135_133": { "cost": 83, "customers": [33]},
		"135_134": { "cost": 45, "customers": []},
		"135_136": { "cost": 87, "customers": []},
		"135_138": { "cost": 69, "customers": [33]},
		"136_127": { "cost": 62, "customers": [18]},
		"136_135": { "cost": 82, "customers": []},
		"136_137": { "cost": 41, "customers": [18]},
		"136_138": { "cost": 84, "customers": []},
		"137_136": { "cost": 42, "customers": [18]},
		"137_141": { "cost": 128, "customers": []},
		"138_135": { "cost": 75, "customers": [33]},
		"138_136": { "cost": 85, "customers": []},
		"138_139": { "cost": 40, "customers": []},
		"139_138": { "cost": 49, "customers": []},
		"139_140": { "cost": 50, "customers": [34]},
		"139_147": { "cost": 103, "customers": [34]},
		"140_137": { "cost": 87, "customers": []},
		"140_139": { "cost": 48, "customers": [34]},
		"140_141": { "cost": 70, "customers": []},
		"141_140": { "cost": 70, "customers": []},
		"141_142": { "cost": 40, "customers": []},
		"141_145": { "cost": 45, "customers": [35]},
		"142_126": { "cost": 74, "customers": []},
		"142_141": { "cost": 42, "customers": []},
		"142_143": { "cost": 88, "customers": [36]},
		"142_146": { "cost": 43, "customers": []},
		"143_125": { "cost": 59, "customers": []},
		"143_142": { "cost": 85, "customers": [36]},
		"143_151": { "cost": 70, "customers": []},
		"144_122": { "cost": 101, "customers": []},
		"144_124": { "cost": 52, "customers": [31]},
		"144_143": { "cost": 87, "customers": []},
		"145_141": { "cost": 54, "customers": [35]},
		"145_148": { "cost": 53, "customers": []},
		"146_142": { "cost": 47, "customers": []},
		"146_145": { "cost": 77, "customers": [35]},
		"146_150": { "cost": 62, "customers": []},
		"147_0": { "cost": 113, "customers": [34]},
		"147_139": { "cost": 103, "customers": [34]},
		"147_148": { "cost": 40, "customers": []},
		"148_145": { "cost": 58, "customers": []},
		"148_147": { "cost": 35, "customers": []},
		"148_149": { "cost": 22, "customers": [37]},
		"149_148": { "cost": 26, "customers": [37]},
		"150_146": { "cost": 64, "customers": []},
		"150_149": { "cost": 84, "customers": [37]},
		"150_183": { "cost": 86, "customers": []},
		"151_143": { "cost": 77, "customers": []},
		"151_152": { "cost": 113, "customers": [38]},
		"151_155": { "cost": 77, "customers": [38]},
		"152_121": { "cost": 88, "customers": []},
		"152_155": { "cost": 124, "customers": []},
		"153_149": { "cost": 37, "customers": []},
		"153_157": { "cost": 88, "customers": [39]},
		"154_150": { "cost": 46, "customers": []},
		"154_153": { "cost": 66, "customers": []},
		"154_159": { "cost": 65, "customers": [40]},
		"155_151": { "cost": 73, "customers": [38]},
		"155_152": { "cost": 124, "customers": []},
		"155_160": { "cost": 42, "customers": []},
		"155_183": { "cost": 38, "customers": []},
		"156_1": { "cost": 37, "customers": [1]},
		"157_0": { "cost": 69, "customers": []},
		"157_158": { "cost": 52, "customers": []},
		"158_157": { "cost": 45, "customers": []},
		"158_159": { "cost": 67, "customers": []},
		"158_173": { "cost": 28, "customers": []},
		"158_174": { "cost": 95, "customers": [41]},
		"159_154": { "cost": 61, "customers": [40]},
		"159_158": { "cost": 69, "customers": []},
		"159_183": { "cost": 53, "customers": [40]},
		"160_155": { "cost": 44, "customers": []},
		"160_161": { "cost": 44, "customers": []},
		"160_169": { "cost": 39, "customers": [42]},
		"160_173": { "cost": 131, "customers": []},
		"161_160": { "cost": 41, "customers": []},
		"161_162": { "cost": 126, "customers": [43]},
		"162_120": { "cost": 46, "customers": []},
		"162_163": { "cost": 140, "customers": []},
		"162_167": { "cost": 52, "customers": []},
		"163_119": { "cost": 65, "customers": [29]},
		"163_164": { "cost": 61, "customers": []},
		"163_166": { "cost": 80, "customers": []},
		"164_163": { "cost": 68, "customers": []},
		"164_165": { "cost": 72, "customers": [44]},
		"165_164": { "cost": 76, "customers": [44]},
		"166_163": { "cost": 75, "customers": []},
		"166_165": { "cost": 93, "customers": []},
		"166_167": { "cost": 148, "customers": [43]},
		"167_161": { "cost": 111, "customers": [43]},
		"167_162": { "cost": 50, "customers": []},
		"167_168": { "cost": 55, "customers": [43]},
		"168_167": { "cost": 58, "customers": [43]},
		"168_169": { "cost": 136, "customers": []},
		"168_184": { "cost": 40, "customers": [43]},
		"169_160": { "cost": 37, "customers": [42]},
		"169_168": { "cost": 141, "customers": []},
		"169_170": { "cost": 38, "customers": [42]},
		"170_169": { "cost": 39, "customers": [42]},
		"171_172": { "cost": 49, "customers": [45]},
		"172_171": { "cost": 45, "customers": [45]},
		"172_173": { "cost": 33, "customers": [45]},
		"172_175": { "cost": 84, "customers": []},
		"172_181": { "cost": 60, "customers": [45]},
		"173_158": { "cost": 25, "customers": []},
		"173_160": { "cost": 137, "customers": []},
		"173_172": { "cost": 27, "customers": [45]},
		"174_158": { "cost": 92, "customers": [41]},
		"174_175": { "cost": 53, "customers": [41]},
		"175_172": { "cost": 85, "customers": []},
		"175_178": { "cost": 129, "customers": [41]},
		"176_174": { "cost": 105, "customers": []},
		"176_177": { "cost": 96, "customers": [46]},
		"177_176": { "cost": 97, "customers": [46]},
		"177_178": { "cost": 52, "customers": []},
		"178_175": { "cost": 133, "customers": [41]},
		"178_179": { "cost": 64, "customers": []},
		"179_180": { "cost": 62, "customers": []},
		"179_181": { "cost": 204, "customers": [45]},
		"180_182": { "cost": 188, "customers": [47]},
		"181_172": { "cost": 53, "customers": [45]},
		"181_179": { "cost": 201, "customers": [45]},
		"181_182": { "cost": 64, "customers": []},
		"182_181": { "cost": 58, "customers": []},
		"183_150": { "cost": 95, "customers": []},
		"183_155": { "cost": 30, "customers": []},
		"183_159": { "cost": 45, "customers": [40]},
		"184_168": { "cost": 40, "customers": [43]},
		"185_123": { "cost": 47, "customers": [30]},
		"185_124": { "cost": 48, "customers": []},
		"186_128": { "cost": 79, "customers": []},
		"186_129": { "cost": 120, "customers": []},
		"186_132": { "cost": 80, "customers": [33]},
		"187_97": { "cost": 33, "customers": []},
		"187_105": { "cost": 42, "customers": [26]},
		"187_106": { "cost": 35, "customers": [26]},
		"188_63": { "cost": 64, "customers": [14]},
		"188_74": { "cost": 58, "customers": [14]},
		"189_79": { "cost": 39, "customers": []},
		"189_80": { "cost": 41, "customers": [22]},
		"189_81": { "cost": 80, "customers": []},
		"189_95": { "cost": 90, "customers": [22]},
		"190_15": { "cost": 32, "customers": [4]},
		"191_18": { "cost": 35, "customers": [3]},
		"191_33": { "cost": 45, "customers": [3]},
		"191_34": { "cost": 38, "customers": [3]},
		"192_6": { "cost": 75, "customers": []},
		"192_16": { "cost": 80, "customers": [3]},
		"193_11": { "cost": 66, "customers": []},
		"193_12": { "cost": 39, "customers": [7]},
		"193_194": { "cost": 71, "customers": [7]},
		"194_11": { "cost": 35, "customers": []},
		"195_8": { "cost": 50, "customers": [5]},
		"195_9": { "cost": 49, "customers": []},
		"195_45": { "cost": 78, "customers": []}
	}
};