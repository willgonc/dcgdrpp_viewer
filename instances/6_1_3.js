if (typeof instances == "undefined")
	window.instances = {};
instances["M3103_gdrpp_05_3"] = {
	maxDistance: 4243,
	totalArcs: 537,
	totalNodes: 196,
	totalCustomers: 42,
	totalVehicles: 3,
	arcs: {
		"0_3": { "cost": 68, "customers": [2]},
		"1_2": { "cost": 49, "customers": []},
		"1_24": { "cost": 58, "customers": [1]},
		"2_21": { "cost": 40, "customers": []},
		"2_23": { "cost": 60, "customers": []},
		"3_2": { "cost": 70, "customers": [2]},
		"3_19": { "cost": 62, "customers": []},
		"4_1": { "cost": 47, "customers": [1]},
		"4_25": { "cost": 49, "customers": []},
		"5_0": { "cost": 53, "customers": [2]},
		"5_192": { "cost": 61, "customers": []},
		"6_7": { "cost": 104, "customers": []},
		"6_14": { "cost": 59, "customers": [3]},
		"6_192": { "cost": 68, "customers": [3]},
		"7_193": { "cost": 91, "customers": [4]},
		"8_9": { "cost": 25, "customers": []},
		"9_8": { "cost": 33, "customers": []},
		"9_44": { "cost": 35, "customers": []},
		"10_9": { "cost": 54, "customers": [5]},
		"10_42": { "cost": 36, "customers": []},
		"11_10": { "cost": 34, "customers": []},
		"11_12": { "cost": 37, "customers": []},
		"11_40": { "cost": 58, "customers": []},
		"11_194": { "cost": 47, "customers": []},
		"12_11": { "cost": 48, "customers": []},
		"12_13": { "cost": 36, "customers": []},
		"12_39": { "cost": 55, "customers": []},
		"13_12": { "cost": 39, "customers": []},
		"13_14": { "cost": 69, "customers": []},
		"13_38": { "cost": 63, "customers": [4]},
		"13_193": { "cost": 35, "customers": [4]},
		"14_6": { "cost": 65, "customers": [3]},
		"14_7": { "cost": 87, "customers": []},
		"14_13": { "cost": 72, "customers": []},
		"14_15": { "cost": 108, "customers": [3]},
		"15_14": { "cost": 97, "customers": [3]},
		"15_16": { "cost": 46, "customers": []},
		"15_190": { "cost": 29, "customers": [3]},
		"16_15": { "cost": 42, "customers": []},
		"16_17": { "cost": 42, "customers": []},
		"16_192": { "cost": 83, "customers": []},
		"17_5": { "cost": 87, "customers": []},
		"17_16": { "cost": 51, "customers": []},
		"17_18": { "cost": 32, "customers": [7]},
		"18_0": { "cost": 118, "customers": []},
		"18_17": { "cost": 34, "customers": [7]},
		"18_20": { "cost": 45, "customers": []},
		"18_191": { "cost": 45, "customers": []},
		"19_3": { "cost": 58, "customers": []},
		"19_20": { "cost": 65, "customers": [8]},
		"20_18": { "cost": 44, "customers": []},
		"20_19": { "cost": 52, "customers": [8]},
		"20_22": { "cost": 64, "customers": [8]},
		"20_33": { "cost": 79, "customers": [8]},
		"21_19": { "cost": 33, "customers": [8]},
		"21_22": { "cost": 65, "customers": []},
		"22_20": { "cost": 57, "customers": [8]},
		"22_21": { "cost": 65, "customers": []},
		"22_23": { "cost": 56, "customers": [8]},
		"22_32": { "cost": 67, "customers": [8]},
		"23_2": { "cost": 70, "customers": []},
		"23_22": { "cost": 51, "customers": [8]},
		"23_24": { "cost": 55, "customers": []},
		"23_32": { "cost": 60, "customers": []},
		"24_1": { "cost": 50, "customers": [1]},
		"24_23": { "cost": 55, "customers": []},
		"24_30": { "cost": 125, "customers": []},
		"25_4": { "cost": 42, "customers": []},
		"25_24": { "cost": 44, "customers": [1]},
		"25_26": { "cost": 45, "customers": [1]},
		"25_29": { "cost": 154, "customers": [1]},
		"26_25": { "cost": 52, "customers": [1]},
		"26_27": { "cost": 132, "customers": [1]},
		"27_28": { "cost": 51, "customers": []},
		"27_29": { "cost": 96, "customers": []},
		"28_29": { "cost": 101, "customers": []},
		"28_61": { "cost": 52, "customers": []},
		"28_70": { "cost": 228, "customers": [9]},
		"29_25": { "cost": 150, "customers": [1]},
		"29_27": { "cost": 96, "customers": []},
		"29_28": { "cost": 92, "customers": []},
		"29_30": { "cost": 44, "customers": []},
		"29_59": { "cost": 57, "customers": [1]},
		"29_60": { "cost": 51, "customers": []},
		"30_29": { "cost": 43, "customers": []},
		"30_31": { "cost": 44, "customers": [10]},
		"31_30": { "cost": 46, "customers": [10]},
		"31_32": { "cost": 27, "customers": []},
		"31_58": { "cost": 43, "customers": [10]},
		"32_22": { "cost": 81, "customers": [8]},
		"32_23": { "cost": 62, "customers": []},
		"32_31": { "cost": 34, "customers": []},
		"33_20": { "cost": 78, "customers": [8]},
		"33_35": { "cost": 79, "customers": [8]},
		"33_57": { "cost": 39, "customers": [8]},
		"33_191": { "cost": 37, "customers": [8]},
		"34_16": { "cost": 128, "customers": [6]},
		"34_17": { "cost": 69, "customers": []},
		"34_191": { "cost": 29, "customers": []},
		"35_33": { "cost": 80, "customers": [8]},
		"36_37": { "cost": 47, "customers": []},
		"37_38": { "cost": 67, "customers": [4]},
		"37_49": { "cost": 116, "customers": [4]},
		"37_52": { "cost": 69, "customers": []},
		"38_13": { "cost": 69, "customers": [4]},
		"38_37": { "cost": 81, "customers": [4]},
		"39_38": { "cost": 42, "customers": []},
		"39_40": { "cost": 38, "customers": [12]},
		"40_11": { "cost": 58, "customers": []},
		"40_39": { "cost": 46, "customers": [12]},
		"40_41": { "cost": 32, "customers": []},
		"40_49": { "cost": 73, "customers": []},
		"41_40": { "cost": 31, "customers": []},
		"41_42": { "cost": 28, "customers": []},
		"41_43": { "cost": 36, "customers": [13]},
		"42_41": { "cost": 36, "customers": []},
		"42_44": { "cost": 34, "customers": [13]},
		"43_41": { "cost": 35, "customers": [13]},
		"43_44": { "cost": 26, "customers": [13]},
		"43_45": { "cost": 34, "customers": []},
		"43_48": { "cost": 83, "customers": []},
		"44_9": { "cost": 35, "customers": []},
		"44_42": { "cost": 29, "customers": [13]},
		"44_43": { "cost": 26, "customers": [13]},
		"45_47": { "cost": 82, "customers": [14]},
		"45_195": { "cost": 72, "customers": []},
		"46_47": { "cost": 71, "customers": []},
		"46_86": { "cost": 103, "customers": [15]},
		"47_46": { "cost": 76, "customers": []},
		"47_48": { "cost": 40, "customers": [14]},
		"48_43": { "cost": 83, "customers": []},
		"48_47": { "cost": 38, "customers": [14]},
		"48_49": { "cost": 55, "customers": []},
		"49_40": { "cost": 59, "customers": []},
		"49_48": { "cost": 58, "customers": []},
		"49_51": { "cost": 49, "customers": []},
		"50_46": { "cost": 113, "customers": []},
		"50_83": { "cost": 67, "customers": []},
		"51_49": { "cost": 52, "customers": []},
		"51_50": { "cost": 39, "customers": [16]},
		"51_54": { "cost": 51, "customers": [16]},
		"52_37": { "cost": 66, "customers": []},
		"52_51": { "cost": 95, "customers": []},
		"52_53": { "cost": 33, "customers": [11]},
		"52_55": { "cost": 38, "customers": []},
		"53_36": { "cost": 67, "customers": [11]},
		"53_52": { "cost": 36, "customers": [11]},
		"53_56": { "cost": 46, "customers": []},
		"54_51": { "cost": 43, "customers": [16]},
		"54_55": { "cost": 71, "customers": [16]},
		"54_82": { "cost": 15, "customers": []},
		"55_52": { "cost": 52, "customers": []},
		"55_54": { "cost": 82, "customers": [16]},
		"55_56": { "cost": 37, "customers": [16]},
		"55_80": { "cost": 85, "customers": []},
		"56_55": { "cost": 44, "customers": [16]},
		"56_57": { "cost": 125, "customers": []},
		"57_33": { "cost": 51, "customers": [8]},
		"57_56": { "cost": 123, "customers": []},
		"57_67": { "cost": 43, "customers": []},
		"58_31": { "cost": 48, "customers": [10]},
		"58_59": { "cost": 42, "customers": []},
		"59_29": { "cost": 59, "customers": [1]},
		"59_58": { "cost": 38, "customers": []},
		"59_65": { "cost": 60, "customers": [1]},
		"59_66": { "cost": 65, "customers": [1]},
		"60_29": { "cost": 57, "customers": []},
		"60_64": { "cost": 63, "customers": [17]},
		"61_60": { "cost": 93, "customers": []},
		"61_62": { "cost": 60, "customers": [9]},
		"62_61": { "cost": 55, "customers": [9]},
		"62_63": { "cost": 42, "customers": [9]},
		"62_73": { "cost": 118, "customers": [9]},
		"63_62": { "cost": 43, "customers": [9]},
		"63_64": { "cost": 59, "customers": []},
		"63_188": { "cost": 57, "customers": [9]},
		"64_63": { "cost": 52, "customers": []},
		"64_65": { "cost": 43, "customers": []},
		"65_59": { "cost": 54, "customers": [1]},
		"65_64": { "cost": 45, "customers": []},
		"65_66": { "cost": 56, "customers": []},
		"65_75": { "cost": 117, "customers": [1]},
		"66_59": { "cost": 62, "customers": [1]},
		"66_65": { "cost": 55, "customers": []},
		"66_67": { "cost": 113, "customers": []},
		"66_77": { "cost": 142, "customers": []},
		"67_57": { "cost": 52, "customers": []},
		"67_66": { "cost": 111, "customers": []},
		"67_68": { "cost": 46, "customers": [18]},
		"68_67": { "cost": 48, "customers": [18]},
		"68_78": { "cost": 66, "customers": [18]},
		"69_56": { "cost": 70, "customers": []},
		"69_68": { "cost": 82, "customers": [18]},
		"70_28": { "cost": 231, "customers": [9]},
		"70_101": { "cost": 86, "customers": []},
		"71_70": { "cost": 40, "customers": [9]},
		"71_102": { "cost": 91, "customers": []},
		"72_71": { "cost": 41, "customers": [9]},
		"72_73": { "cost": 36, "customers": [9]},
		"72_103": { "cost": 97, "customers": []},
		"73_62": { "cost": 122, "customers": [9]},
		"73_72": { "cost": 46, "customers": [9]},
		"73_74": { "cost": 54, "customers": []},
		"73_99": { "cost": 41, "customers": [9]},
		"74_73": { "cost": 53, "customers": []},
		"74_75": { "cost": 91, "customers": []},
		"75_65": { "cost": 108, "customers": [1]},
		"75_74": { "cost": 82, "customers": []},
		"75_76": { "cost": 83, "customers": []},
		"75_98": { "cost": 66, "customers": []},
		"76_75": { "cost": 79, "customers": []},
		"76_77": { "cost": 82, "customers": []},
		"76_97": { "cost": 67, "customers": [19]},
		"77_66": { "cost": 148, "customers": []},
		"77_76": { "cost": 92, "customers": []},
		"77_78": { "cost": 31, "customers": [18]},
		"77_96": { "cost": 131, "customers": []},
		"78_68": { "cost": 58, "customers": [18]},
		"78_77": { "cost": 27, "customers": [18]},
		"78_79": { "cost": 86, "customers": []},
		"78_109": { "cost": 187, "customers": []},
		"79_69": { "cost": 52, "customers": [18]},
		"79_78": { "cost": 96, "customers": []},
		"79_80": { "cost": 40, "customers": []},
		"80_55": { "cost": 85, "customers": []},
		"80_79": { "cost": 48, "customers": []},
		"80_81": { "cost": 48, "customers": [20]},
		"80_189": { "cost": 40, "customers": [20]},
		"81_80": { "cost": 46, "customers": [20]},
		"81_82": { "cost": 41, "customers": [20]},
		"81_90": { "cost": 160, "customers": [20]},
		"81_189": { "cost": 82, "customers": []},
		"82_54": { "cost": 16, "customers": []},
		"82_81": { "cost": 52, "customers": [20]},
		"82_83": { "cost": 48, "customers": [20]},
		"83_82": { "cost": 42, "customers": [20]},
		"83_84": { "cost": 36, "customers": []},
		"84_83": { "cost": 48, "customers": []},
		"84_85": { "cost": 85, "customers": [21]},
		"84_86": { "cost": 115, "customers": []},
		"85_84": { "cost": 78, "customers": [21]},
		"86_46": { "cost": 105, "customers": [15]},
		"86_84": { "cost": 122, "customers": []},
		"86_89": { "cost": 36, "customers": []},
		"87_88": { "cost": 196, "customers": [22]},
		"88_87": { "cost": 208, "customers": [22]},
		"88_89": { "cost": 58, "customers": [22]},
		"88_116": { "cost": 153, "customers": [22]},
		"89_86": { "cost": 27, "customers": []},
		"89_88": { "cost": 52, "customers": [22]},
		"89_90": { "cost": 51, "customers": []},
		"90_81": { "cost": 167, "customers": [20]},
		"90_89": { "cost": 47, "customers": []},
		"90_91": { "cost": 141, "customers": []},
		"90_92": { "cost": 52, "customers": [20]},
		"91_81": { "cost": 104, "customers": []},
		"91_90": { "cost": 135, "customers": []},
		"91_94": { "cost": 57, "customers": [23]},
		"92_114": { "cost": 138, "customers": []},
		"93_92": { "cost": 112, "customers": []},
		"93_94": { "cost": 84, "customers": []},
		"94_91": { "cost": 52, "customers": [23]},
		"94_93": { "cost": 88, "customers": []},
		"94_95": { "cost": 42, "customers": []},
		"95_94": { "cost": 51, "customers": []},
		"95_109": { "cost": 128, "customers": []},
		"95_189": { "cost": 92, "customers": [20]},
		"96_77": { "cost": 126, "customers": []},
		"96_97": { "cost": 103, "customers": [19]},
		"96_108": { "cost": 41, "customers": [19]},
		"97_76": { "cost": 70, "customers": [19]},
		"97_96": { "cost": 112, "customers": [19]},
		"97_187": { "cost": 30, "customers": [19]},
		"98_75": { "cost": 66, "customers": []},
		"98_99": { "cost": 126, "customers": []},
		"98_105": { "cost": 35, "customers": [25]},
		"99_73": { "cost": 37, "customers": [9]},
		"99_98": { "cost": 129, "customers": []},
		"99_104": { "cost": 69, "customers": []},
		"100_101": { "cost": 286, "customers": [26]},
		"101_70": { "cost": 95, "customers": []},
		"101_102": { "cost": 44, "customers": [26]},
		"101_128": { "cost": 88, "customers": [26]},
		"102_103": { "cost": 43, "customers": []},
		"103_72": { "cost": 85, "customers": []},
		"103_102": { "cost": 42, "customers": []},
		"103_104": { "cost": 84, "customers": [27]},
		"103_128": { "cost": 118, "customers": []},
		"104_99": { "cost": 63, "customers": []},
		"104_103": { "cost": 92, "customers": [27]},
		"104_105": { "cost": 138, "customers": []},
		"104_126": { "cost": 133, "customers": []},
		"104_127": { "cost": 64, "customers": []},
		"105_98": { "cost": 40, "customers": [25]},
		"105_104": { "cost": 151, "customers": []},
		"105_126": { "cost": 145, "customers": [25]},
		"105_187": { "cost": 34, "customers": []},
		"106_107": { "cost": 101, "customers": []},
		"106_125": { "cost": 133, "customers": [19]},
		"106_187": { "cost": 28, "customers": [19]},
		"107_106": { "cost": 94, "customers": []},
		"107_109": { "cost": 57, "customers": []},
		"107_124": { "cost": 99, "customers": [19]},
		"108_96": { "cost": 48, "customers": [19]},
		"109_78": { "cost": 199, "customers": []},
		"109_95": { "cost": 130, "customers": []},
		"109_107": { "cost": 53, "customers": []},
		"109_110": { "cost": 107, "customers": [28]},
		"109_123": { "cost": 83, "customers": []},
		"110_109": { "cost": 108, "customers": [28]},
		"110_111": { "cost": 53, "customers": [28]},
		"110_112": { "cost": 70, "customers": [28]},
		"110_123": { "cost": 105, "customers": []},
		"111_94": { "cost": 128, "customers": []},
		"111_112": { "cost": 49, "customers": []},
		"112_110": { "cost": 64, "customers": [28]},
		"112_111": { "cost": 49, "customers": []},
		"112_113": { "cost": 79, "customers": []},
		"112_119": { "cost": 101, "customers": [28]},
		"112_121": { "cost": 130, "customers": []},
		"113_93": { "cost": 117, "customers": [24]},
		"113_112": { "cost": 83, "customers": []},
		"113_114": { "cost": 48, "customers": [24]},
		"114_113": { "cost": 37, "customers": [24]},
		"114_115": { "cost": 82, "customers": []},
		"114_118": { "cost": 149, "customers": [24]},
		"115_114": { "cost": 88, "customers": []},
		"115_116": { "cost": 41, "customers": [22]},
		"116_88": { "cost": 153, "customers": [22]},
		"116_115": { "cost": 47, "customers": [22]},
		"116_117": { "cost": 106, "customers": []},
		"117_87": { "cost": 135, "customers": [22]},
		"117_116": { "cost": 105, "customers": []},
		"118_114": { "cost": 156, "customers": [24]},
		"118_115": { "cost": 72, "customers": []},
		"119_112": { "cost": 102, "customers": [28]},
		"119_163": { "cost": 64, "customers": [28]},
		"120_119": { "cost": 137, "customers": [28]},
		"120_121": { "cost": 43, "customers": [28]},
		"120_162": { "cost": 43, "customers": []},
		"121_112": { "cost": 122, "customers": []},
		"121_120": { "cost": 47, "customers": [28]},
		"121_122": { "cost": 30, "customers": [28]},
		"121_152": { "cost": 93, "customers": []},
		"122_121": { "cost": 24, "customers": [28]},
		"122_123": { "cost": 53, "customers": [28]},
		"122_144": { "cost": 107, "customers": [28]},
		"123_109": { "cost": 78, "customers": []},
		"123_122": { "cost": 45, "customers": [28]},
		"123_185": { "cost": 54, "customers": []},
		"124_107": { "cost": 99, "customers": [19]},
		"124_125": { "cost": 75, "customers": [19]},
		"124_144": { "cost": 50, "customers": []},
		"124_185": { "cost": 53, "customers": [19]},
		"125_106": { "cost": 142, "customers": [19]},
		"125_124": { "cost": 72, "customers": [19]},
		"125_126": { "cost": 79, "customers": []},
		"125_143": { "cost": 62, "customers": [19]},
		"126_104": { "cost": 146, "customers": []},
		"126_105": { "cost": 146, "customers": [25]},
		"126_125": { "cost": 75, "customers": []},
		"126_142": { "cost": 72, "customers": [25]},
		"127_104": { "cost": 57, "customers": []},
		"127_134": { "cost": 136, "customers": [29]},
		"127_136": { "cost": 58, "customers": [29]},
		"128_101": { "cost": 82, "customers": [26]},
		"128_103": { "cost": 116, "customers": []},
		"128_134": { "cost": 83, "customers": []},
		"128_186": { "cost": 71, "customers": [26]},
		"129_100": { "cost": 163, "customers": []},
		"129_130": { "cost": 219, "customers": [30]},
		"129_186": { "cost": 122, "customers": []},
		"130_129": { "cost": 214, "customers": [30]},
		"131_129": { "cost": 114, "customers": [30]},
		"131_132": { "cost": 98, "customers": [30]},
		"132_131": { "cost": 111, "customers": [30]},
		"132_133": { "cost": 72, "customers": []},
		"132_134": { "cost": 62, "customers": []},
		"132_186": { "cost": 88, "customers": []},
		"133_135": { "cost": 80, "customers": [31]},
		"134_127": { "cost": 134, "customers": [29]},
		"134_128": { "cost": 95, "customers": []},
		"134_132": { "cost": 70, "customers": []},
		"134_135": { "cost": 42, "customers": []},
		"135_133": { "cost": 78, "customers": [31]},
		"135_134": { "cost": 48, "customers": []},
		"135_136": { "cost": 82, "customers": []},
		"135_138": { "cost": 64, "customers": []},
		"136_127": { "cost": 64, "customers": [29]},
		"136_135": { "cost": 83, "customers": []},
		"136_137": { "cost": 35, "customers": []},
		"136_138": { "cost": 89, "customers": []},
		"137_136": { "cost": 39, "customers": []},
		"138_135": { "cost": 65, "customers": []},
		"138_136": { "cost": 89, "customers": []},
		"138_139": { "cost": 42, "customers": [32]},
		"139_138": { "cost": 39, "customers": [32]},
		"139_140": { "cost": 53, "customers": [32]},
		"139_147": { "cost": 99, "customers": []},
		"140_137": { "cost": 96, "customers": []},
		"140_139": { "cost": 51, "customers": [32]},
		"140_141": { "cost": 78, "customers": []},
		"141_137": { "cost": 126, "customers": [25]},
		"141_140": { "cost": 77, "customers": []},
		"141_142": { "cost": 37, "customers": [25]},
		"141_145": { "cost": 52, "customers": []},
		"142_126": { "cost": 62, "customers": [25]},
		"142_141": { "cost": 49, "customers": [25]},
		"142_143": { "cost": 97, "customers": []},
		"143_125": { "cost": 60, "customers": [19]},
		"143_142": { "cost": 84, "customers": []},
		"143_151": { "cost": 77, "customers": []},
		"144_122": { "cost": 96, "customers": [28]},
		"144_143": { "cost": 81, "customers": []},
		"145_141": { "cost": 54, "customers": []},
		"145_146": { "cost": 71, "customers": [25]},
		"145_148": { "cost": 50, "customers": []},
		"146_142": { "cost": 42, "customers": [25]},
		"147_139": { "cost": 95, "customers": []},
		"147_148": { "cost": 44, "customers": [33]},
		"147_156": { "cost": 115, "customers": []},
		"148_145": { "cost": 52, "customers": []},
		"148_147": { "cost": 44, "customers": [33]},
		"148_149": { "cost": 30, "customers": []},
		"149_148": { "cost": 29, "customers": []},
		"150_146": { "cost": 62, "customers": []},
		"150_149": { "cost": 78, "customers": []},
		"151_143": { "cost": 81, "customers": []},
		"151_155": { "cost": 82, "customers": [35]},
		"152_121": { "cost": 80, "customers": []},
		"152_151": { "cost": 109, "customers": []},
		"152_155": { "cost": 120, "customers": [35]},
		"153_149": { "cost": 36, "customers": [34]},
		"153_157": { "cost": 96, "customers": []},
		"154_150": { "cost": 46, "customers": []},
		"154_153": { "cost": 78, "customers": [34]},
		"155_151": { "cost": 75, "customers": [35]},
		"155_152": { "cost": 113, "customers": [35]},
		"155_160": { "cost": 53, "customers": []},
		"155_183": { "cost": 38, "customers": [35]},
		"156_147": { "cost": 111, "customers": []},
		"156_157": { "cost": 57, "customers": []},
		"156_176": { "cost": 116, "customers": [36]},
		"157_156": { "cost": 66, "customers": []},
		"157_158": { "cost": 55, "customers": [37]},
		"158_157": { "cost": 57, "customers": [37]},
		"158_159": { "cost": 75, "customers": [37]},
		"158_173": { "cost": 27, "customers": []},
		"158_174": { "cost": 91, "customers": []},
		"159_154": { "cost": 65, "customers": []},
		"159_158": { "cost": 72, "customers": [37]},
		"159_183": { "cost": 50, "customers": []},
		"160_155": { "cost": 40, "customers": []},
		"160_161": { "cost": 47, "customers": [38]},
		"160_169": { "cost": 41, "customers": []},
		"160_173": { "cost": 133, "customers": [38]},
		"161_160": { "cost": 32, "customers": [38]},
		"161_162": { "cost": 126, "customers": []},
		"162_120": { "cost": 46, "customers": []},
		"162_161": { "cost": 134, "customers": []},
		"162_163": { "cost": 142, "customers": [28]},
		"162_167": { "cost": 57, "customers": []},
		"163_119": { "cost": 61, "customers": [28]},
		"163_164": { "cost": 58, "customers": [28]},
		"163_166": { "cost": 78, "customers": []},
		"164_163": { "cost": 72, "customers": [28]},
		"164_165": { "cost": 82, "customers": []},
		"165_164": { "cost": 73, "customers": []},
		"166_163": { "cost": 73, "customers": []},
		"166_165": { "cost": 93, "customers": [39]},
		"166_167": { "cost": 154, "customers": [39]},
		"167_161": { "cost": 116, "customers": []},
		"167_162": { "cost": 53, "customers": []},
		"167_168": { "cost": 59, "customers": [39]},
		"168_167": { "cost": 56, "customers": [39]},
		"168_169": { "cost": 134, "customers": [39]},
		"168_184": { "cost": 41, "customers": [39]},
		"169_160": { "cost": 35, "customers": []},
		"169_170": { "cost": 27, "customers": [39]},
		"170_169": { "cost": 34, "customers": [39]},
		"171_172": { "cost": 43, "customers": [40]},
		"172_171": { "cost": 55, "customers": [40]},
		"172_173": { "cost": 22, "customers": []},
		"172_175": { "cost": 80, "customers": []},
		"172_181": { "cost": 62, "customers": []},
		"173_158": { "cost": 26, "customers": []},
		"173_160": { "cost": 128, "customers": [38]},
		"173_172": { "cost": 28, "customers": []},
		"174_158": { "cost": 82, "customers": []},
		"174_175": { "cost": 50, "customers": [41]},
		"175_172": { "cost": 79, "customers": []},
		"175_178": { "cost": 125, "customers": [41]},
		"176_156": { "cost": 108, "customers": [36]},
		"176_174": { "cost": 96, "customers": []},
		"176_177": { "cost": 100, "customers": [36]},
		"177_176": { "cost": 96, "customers": [36]},
		"177_178": { "cost": 43, "customers": []},
		"178_175": { "cost": 136, "customers": [41]},
		"178_179": { "cost": 53, "customers": []},
		"179_180": { "cost": 73, "customers": [42]},
		"179_181": { "cost": 198, "customers": [42]},
		"180_182": { "cost": 191, "customers": [42]},
		"181_172": { "cost": 50, "customers": []},
		"181_179": { "cost": 193, "customers": [42]},
		"181_182": { "cost": 66, "customers": []},
		"182_181": { "cost": 60, "customers": []},
		"183_150": { "cost": 94, "customers": [35]},
		"183_155": { "cost": 36, "customers": [35]},
		"183_159": { "cost": 45, "customers": []},
		"184_168": { "cost": 42, "customers": [39]},
		"185_123": { "cost": 42, "customers": []},
		"185_124": { "cost": 52, "customers": [19]},
		"186_128": { "cost": 81, "customers": [26]},
		"186_129": { "cost": 125, "customers": []},
		"187_97": { "cost": 32, "customers": [19]},
		"187_105": { "cost": 46, "customers": []},
		"187_106": { "cost": 27, "customers": [19]},
		"188_63": { "cost": 59, "customers": [9]},
		"188_74": { "cost": 68, "customers": [9]},
		"189_79": { "cost": 42, "customers": []},
		"189_80": { "cost": 38, "customers": [20]},
		"189_81": { "cost": 75, "customers": []},
		"189_95": { "cost": 82, "customers": [20]},
		"190_15": { "cost": 39, "customers": [3]},
		"191_18": { "cost": 39, "customers": []},
		"191_33": { "cost": 33, "customers": [8]},
		"191_34": { "cost": 34, "customers": []},
		"192_6": { "cost": 70, "customers": [3]},
		"192_16": { "cost": 91, "customers": []},
		"193_11": { "cost": 73, "customers": [4]},
		"193_12": { "cost": 36, "customers": [4]},
		"193_13": { "cost": 33, "customers": [4]},
		"193_194": { "cost": 73, "customers": [4]},
		"194_8": { "cost": 63, "customers": []},
		"194_11": { "cost": 45, "customers": []},
		"195_8": { "cost": 57, "customers": [5]},
		"195_9": { "cost": 45, "customers": [5]}
	}
};