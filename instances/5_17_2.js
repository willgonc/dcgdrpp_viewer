if (typeof instances == "undefined")
	window.instances = {};
instances["A5111_52_gdrpp_2"] = {
	maxDistance: 11193,
	totalArcs: 266,
	totalNodes: 116,
	totalCustomers: 18,
	totalVehicles: 2,
	arcs: {
		"0_50": { "cost": 485, "customers": [4]},
		"0_59": { "cost": 140, "customers": [4]},
		"1_2": { "cost": 182, "customers": [1]},
		"1_51": { "cost": 414, "customers": [1]},
		"2_1": { "cost": 104, "customers": [1]},
		"2_9": { "cost": 269, "customers": [1]},
		"2_11": { "cost": 448, "customers": []},
		"3_6": { "cost": 105, "customers": []},
		"4_3": { "cost": 157, "customers": [2]},
		"5_4": { "cost": 160, "customers": [2]},
		"5_6": { "cost": 39, "customers": [2]},
		"6_3": { "cost": 28, "customers": []},
		"6_5": { "cost": 238, "customers": [2]},
		"6_9": { "cost": 73, "customers": []},
		"7_4": { "cost": 252, "customers": [2]},
		"7_8": { "cost": 56, "customers": []},
		"8_5": { "cost": 162, "customers": [2]},
		"8_7": { "cost": 169, "customers": []},
		"8_9": { "cost": 442, "customers": []},
		"9_2": { "cost": 430, "customers": [1]},
		"9_14": { "cost": 218, "customers": [1]},
		"9_15": { "cost": 17, "customers": []},
		"10_9": { "cost": 85, "customers": []},
		"10_16": { "cost": 1, "customers": [3]},
		"11_2": { "cost": 495, "customers": []},
		"11_12": { "cost": 200, "customers": []},
		"11_29": { "cost": 494, "customers": [4]},
		"12_7": { "cost": 491, "customers": []},
		"12_11": { "cost": 481, "customers": []},
		"12_13": { "cost": 14, "customers": [2]},
		"13_8": { "cost": 157, "customers": [2]},
		"13_14": { "cost": 340, "customers": []},
		"14_18": { "cost": 435, "customers": [1]},
		"15_14": { "cost": 500, "customers": []},
		"15_16": { "cost": 128, "customers": [3]},
		"15_19": { "cost": 342, "customers": []},
		"16_10": { "cost": 177, "customers": [3]},
		"16_17": { "cost": 293, "customers": []},
		"16_20": { "cost": 10, "customers": [3]},
		"17_10": { "cost": 278, "customers": [3]},
		"17_16": { "cost": 191, "customers": []},
		"18_19": { "cost": 154, "customers": []},
		"19_15": { "cost": 203, "customers": []},
		"19_21": { "cost": 217, "customers": [3]},
		"20_17": { "cost": 449, "customers": [3]},
		"20_19": { "cost": 308, "customers": [3]},
		"21_19": { "cost": 212, "customers": [3]},
		"21_22": { "cost": 328, "customers": [3]},
		"21_26": { "cost": 95, "customers": []},
		"22_21": { "cost": 84, "customers": [3]},
		"23_27": { "cost": 174, "customers": [5]},
		"24_18": { "cost": 499, "customers": []},
		"24_31": { "cost": 192, "customers": [6]},
		"25_24": { "cost": 335, "customers": [6]},
		"25_28": { "cost": 234, "customers": []},
		"26_21": { "cost": 146, "customers": []},
		"26_23": { "cost": 181, "customers": []},
		"26_28": { "cost": 48, "customers": [7]},
		"26_33": { "cost": 163, "customers": []},
		"27_38": { "cost": 30, "customers": [5]},
		"28_25": { "cost": 391, "customers": []},
		"28_26": { "cost": 171, "customers": [7]},
		"28_32": { "cost": 246, "customers": [7]},
		"29_11": { "cost": 379, "customers": [4]},
		"29_35": { "cost": 70, "customers": [4]},
		"29_39": { "cost": 109, "customers": [4]},
		"30_24": { "cost": 443, "customers": []},
		"30_31": { "cost": 100, "customers": [6]},
		"30_35": { "cost": 393, "customers": []},
		"31_24": { "cost": 232, "customers": [6]},
		"31_32": { "cost": 370, "customers": []},
		"32_31": { "cost": 67, "customers": []},
		"32_33": { "cost": 63, "customers": [7]},
		"33_26": { "cost": 365, "customers": []},
		"33_36": { "cost": 28, "customers": []},
		"34_38": { "cost": 277, "customers": [5]},
		"35_29": { "cost": 371, "customers": [4]},
		"35_30": { "cost": 39, "customers": []},
		"35_36": { "cost": 69, "customers": [4]},
		"35_40": { "cost": 85, "customers": [4]},
		"36_33": { "cost": 130, "customers": []},
		"36_35": { "cost": 449, "customers": [4]},
		"36_37": { "cost": 415, "customers": [4]},
		"36_43": { "cost": 37, "customers": [4]},
		"37_36": { "cost": 52, "customers": [4]},
		"37_44": { "cost": 25, "customers": [4]},
		"38_27": { "cost": 179, "customers": [5]},
		"38_34": { "cost": 279, "customers": [5]},
		"38_37": { "cost": 330, "customers": []},
		"38_45": { "cost": 16, "customers": []},
		"39_29": { "cost": 263, "customers": [4]},
		"39_40": { "cost": 214, "customers": []},
		"39_55": { "cost": 435, "customers": [4]},
		"40_39": { "cost": 99, "customers": []},
		"40_83": { "cost": 452, "customers": []},
		"41_40": { "cost": 213, "customers": [4]},
		"42_41": { "cost": 324, "customers": [4]},
		"42_43": { "cost": 400, "customers": []},
		"43_36": { "cost": 366, "customers": [4]},
		"43_42": { "cost": 9, "customers": []},
		"43_47": { "cost": 110, "customers": []},
		"43_48": { "cost": 11, "customers": [4]},
		"44_37": { "cost": 391, "customers": [4]},
		"45_37": { "cost": 441, "customers": [4]},
		"45_38": { "cost": 210, "customers": []},
		"45_44": { "cost": 472, "customers": []},
		"45_48": { "cost": 388, "customers": []},
		"46_41": { "cost": 356, "customers": []},
		"46_84": { "cost": 59, "customers": []},
		"47_46": { "cost": 404, "customers": [4]},
		"47_48": { "cost": 46, "customers": []},
		"48_43": { "cost": 136, "customers": [4]},
		"48_45": { "cost": 364, "customers": []},
		"48_87": { "cost": 280, "customers": [4]},
		"49_50": { "cost": 107, "customers": [4]},
		"49_55": { "cost": 457, "customers": [4]},
		"50_0": { "cost": 101, "customers": [4]},
		"50_49": { "cost": 242, "customers": [4]},
		"50_58": { "cost": 234, "customers": [4]},
		"51_1": { "cost": 245, "customers": [1]},
		"52_0": { "cost": 413, "customers": [4]},
		"52_53": { "cost": 378, "customers": [4]},
		"52_60": { "cost": 255, "customers": [4]},
		"53_54": { "cost": 276, "customers": [4]},
		"54_53": { "cost": 237, "customers": [4]},
		"54_61": { "cost": 64, "customers": []},
		"54_62": { "cost": 491, "customers": []},
		"55_39": { "cost": 83, "customers": [4]},
		"55_49": { "cost": 500, "customers": [4]},
		"55_56": { "cost": 450, "customers": [4]},
		"55_62": { "cost": 324, "customers": []},
		"56_55": { "cost": 320, "customers": [4]},
		"56_65": { "cost": 236, "customers": []},
		"57_49": { "cost": 111, "customers": [4]},
		"57_62": { "cost": 158, "customers": []},
		"58_50": { "cost": 480, "customers": [4]},
		"59_58": { "cost": 70, "customers": []},
		"59_60": { "cost": 396, "customers": []},
		"60_52": { "cost": 188, "customers": [4]},
		"60_61": { "cost": 68, "customers": [4]},
		"61_53": { "cost": 290, "customers": []},
		"61_54": { "cost": 198, "customers": []},
		"61_60": { "cost": 377, "customers": [4]},
		"62_54": { "cost": 179, "customers": []},
		"62_55": { "cost": 327, "customers": []},
		"62_57": { "cost": 379, "customers": []},
		"62_63": { "cost": 65, "customers": [8]},
		"62_64": { "cost": 101, "customers": [8]},
		"63_62": { "cost": 99, "customers": [8]},
		"64_62": { "cost": 217, "customers": [8]},
		"64_65": { "cost": 264, "customers": []},
		"65_56": { "cost": 158, "customers": []},
		"65_71": { "cost": 242, "customers": [8]},
		"66_67": { "cost": 459, "customers": [9]},
		"66_96": { "cost": 432, "customers": [9]},
		"67_106": { "cost": 238, "customers": [9]},
		"68_93": { "cost": 187, "customers": []},
		"68_105": { "cost": 66, "customers": [10]},
		"69_70": { "cost": 45, "customers": [8]},
		"70_64": { "cost": 379, "customers": [8]},
		"70_71": { "cost": 159, "customers": [8]},
		"70_75": { "cost": 78, "customers": []},
		"71_65": { "cost": 294, "customers": [8]},
		"71_70": { "cost": 353, "customers": [8]},
		"71_72": { "cost": 127, "customers": [8]},
		"72_76": { "cost": 342, "customers": []},
		"73_74": { "cost": 307, "customers": [8]},
		"74_69": { "cost": 115, "customers": [8]},
		"74_73": { "cost": 90, "customers": [8]},
		"75_70": { "cost": 419, "customers": []},
		"75_76": { "cost": 341, "customers": [11]},
		"75_80": { "cost": 177, "customers": []},
		"76_75": { "cost": 225, "customers": [11]},
		"77_80": { "cost": 402, "customers": [12]},
		"78_74": { "cost": 342, "customers": []},
		"78_79": { "cost": 167, "customers": [8]},
		"78_81": { "cost": 187, "customers": []},
		"79_69": { "cost": 275, "customers": [8]},
		"79_78": { "cost": 423, "customers": [8]},
		"79_80": { "cost": 399, "customers": []},
		"80_75": { "cost": 276, "customers": []},
		"80_77": { "cost": 151, "customers": [12]},
		"80_81": { "cost": 260, "customers": []},
		"81_78": { "cost": 403, "customers": []},
		"81_80": { "cost": 338, "customers": []},
		"81_82": { "cost": 28, "customers": [4]},
		"82_81": { "cost": 300, "customers": [4]},
		"82_93": { "cost": 438, "customers": [4]},
		"82_105": { "cost": 229, "customers": []},
		"83_40": { "cost": 52, "customers": []},
		"83_56": { "cost": 392, "customers": [4]},
		"83_93": { "cost": 430, "customers": [4]},
		"84_46": { "cost": 32, "customers": []},
		"84_83": { "cost": 335, "customers": [4]},
		"84_92": { "cost": 14, "customers": []},
		"85_84": { "cost": 488, "customers": []},
		"85_86": { "cost": 224, "customers": [4]},
		"86_47": { "cost": 236, "customers": [4]},
		"87_86": { "cost": 122, "customers": []},
		"87_90": { "cost": 87, "customers": [4]},
		"87_91": { "cost": 420, "customers": []},
		"88_85": { "cost": 25, "customers": []},
		"89_86": { "cost": 343, "customers": [4]},
		"89_88": { "cost": 90, "customers": [4]},
		"90_87": { "cost": 176, "customers": [4]},
		"90_89": { "cost": 397, "customers": [4]},
		"90_99": { "cost": 74, "customers": []},
		"91_90": { "cost": 434, "customers": [4]},
		"92_83": { "cost": 364, "customers": []},
		"92_93": { "cost": 288, "customers": [4]},
		"92_94": { "cost": 434, "customers": []},
		"93_68": { "cost": 163, "customers": []},
		"93_82": { "cost": 228, "customers": [4]},
		"93_83": { "cost": 259, "customers": [4]},
		"93_92": { "cost": 421, "customers": [4]},
		"94_92": { "cost": 431, "customers": []},
		"94_95": { "cost": 170, "customers": []},
		"95_88": { "cost": 25, "customers": [4]},
		"95_97": { "cost": 88, "customers": []},
		"95_98": { "cost": 88, "customers": []},
		"96_94": { "cost": 310, "customers": [9]},
		"96_97": { "cost": 153, "customers": []},
		"97_95": { "cost": 114, "customers": []},
		"97_96": { "cost": 97, "customers": []},
		"97_98": { "cost": 318, "customers": [13]},
		"98_95": { "cost": 110, "customers": []},
		"98_99": { "cost": 278, "customers": []},
		"99_89": { "cost": 161, "customers": []},
		"99_90": { "cost": 189, "customers": []},
		"99_98": { "cost": 160, "customers": []},
		"99_100": { "cost": 277, "customers": [14]},
		"99_102": { "cost": 14, "customers": [14]},
		"100_99": { "cost": 167, "customers": [14]},
		"101_66": { "cost": 8, "customers": [9]},
		"101_103": { "cost": 422, "customers": []},
		"102_99": { "cost": 217, "customers": [14]},
		"102_101": { "cost": 294, "customers": []},
		"102_104": { "cost": 447, "customers": []},
		"103_101": { "cost": 254, "customers": []},
		"104_102": { "cost": 106, "customers": []},
		"104_103": { "cost": 388, "customers": [15]},
		"104_107": { "cost": 135, "customers": [15]},
		"105_68": { "cost": 372, "customers": [10]},
		"105_82": { "cost": 259, "customers": []},
		"105_106": { "cost": 405, "customers": []},
		"106_67": { "cost": 193, "customers": [9]},
		"106_105": { "cost": 114, "customers": []},
		"106_107": { "cost": 494, "customers": []},
		"107_104": { "cost": 60, "customers": [15]},
		"107_106": { "cost": 90, "customers": []},
		"107_108": { "cost": 82, "customers": [15]},
		"107_109": { "cost": 143, "customers": []},
		"108_107": { "cost": 201, "customers": [15]},
		"109_107": { "cost": 82, "customers": []},
		"109_110": { "cost": 232, "customers": [16]},
		"109_111": { "cost": 420, "customers": []},
		"110_109": { "cost": 286, "customers": [16]},
		"111_109": { "cost": 341, "customers": []},
		"111_112": { "cost": 268, "customers": [17]},
		"111_113": { "cost": 154, "customers": [17]},
		"111_115": { "cost": 195, "customers": []},
		"112_111": { "cost": 249, "customers": [17]},
		"113_111": { "cost": 474, "customers": [17]},
		"113_114": { "cost": 379, "customers": []},
		"114_115": { "cost": 332, "customers": [18]},
		"115_111": { "cost": 399, "customers": []}
	}
};