if (typeof instances == "undefined")
	window.instances = {};
instances["LCGDRPP_75_3_5_1_4"] = {
	maxDistance: 1973,
	totalArcs: 448,
	totalNodes: 75,
	totalCustomers: 37,
	totalVehicles: 4,
	arcs: {
		"0_13": { "cost": 203, "customers": [29]},
		"0_14": { "cost": 279, "customers": [11,29]},
		"0_47": { "cost": 251, "customers": []},
		"0_59": { "cost": 202, "customers": [1]},
		"0_61": { "cost": 88, "customers": [1,29]},
		"0_70": { "cost": 185, "customers": [11]},
		"1_3": { "cost": 164, "customers": [18]},
		"1_5": { "cost": 52, "customers": [18]},
		"1_15": { "cost": 158, "customers": []},
		"1_50": { "cost": 161, "customers": [20]},
		"1_51": { "cost": 106, "customers": [20]},
		"1_55": { "cost": 73, "customers": []},
		"2_13": { "cost": 103, "customers": []},
		"2_14": { "cost": 118, "customers": []},
		"2_27": { "cost": 83, "customers": [19,36]},
		"2_40": { "cost": 98, "customers": [36]},
		"2_42": { "cost": 124, "customers": [19,36]},
		"2_69": { "cost": 151, "customers": [36]},
		"3_1": { "cost": 176, "customers": [18]},
		"3_5": { "cost": 190, "customers": [18]},
		"3_38": { "cost": 119, "customers": []},
		"3_44": { "cost": 114, "customers": []},
		"3_45": { "cost": 196, "customers": []},
		"3_55": { "cost": 189, "customers": [18]},
		"4_18": { "cost": 68, "customers": [4,14,24,35]},
		"4_20": { "cost": 52, "customers": [4,14,21,24]},
		"4_25": { "cost": 109, "customers": [4,14,24]},
		"4_26": { "cost": 305, "customers": [24,28]},
		"4_35": { "cost": 23, "customers": [4,14,24]},
		"4_54": { "cost": 158, "customers": [4,14,21,35]},
		"5_1": { "cost": 59, "customers": [18]},
		"5_15": { "cost": 197, "customers": []},
		"5_32": { "cost": 163, "customers": [18]},
		"5_50": { "cost": 103, "customers": [18,20]},
		"5_51": { "cost": 57, "customers": [20]},
		"5_55": { "cost": 31, "customers": [18]},
		"6_2": { "cost": 143, "customers": []},
		"6_37": { "cost": 60, "customers": [27]},
		"6_49": { "cost": 15, "customers": [2,10,27]},
		"6_60": { "cost": 113, "customers": [10]},
		"6_63": { "cost": 41, "customers": [32]},
		"6_74": { "cost": 63, "customers": [2]},
		"7_11": { "cost": 195, "customers": [7,23]},
		"7_30": { "cost": 115, "customers": [7,23]},
		"7_31": { "cost": 373, "customers": [7]},
		"7_53": { "cost": 138, "customers": [7]},
		"7_56": { "cost": 753, "customers": []},
		"7_67": { "cost": 156, "customers": [7]},
		"8_12": { "cost": 89, "customers": [25]},
		"8_28": { "cost": 188, "customers": [3,25]},
		"8_33": { "cost": 193, "customers": [3,25]},
		"8_43": { "cost": 151, "customers": [3,25]},
		"8_59": { "cost": 161, "customers": []},
		"8_64": { "cost": 30, "customers": [3]},
		"9_8": { "cost": 207, "customers": [3,25]},
		"9_11": { "cost": 166, "customers": [30]},
		"9_33": { "cost": 175, "customers": [3,25,30]},
		"9_43": { "cost": 62, "customers": [25,30]},
		"9_53": { "cost": 289, "customers": []},
		"9_57": { "cost": 47, "customers": [9,30]},
		"10_46": { "cost": 258, "customers": [17]},
		"10_54": { "cost": 258, "customers": []},
		"10_56": { "cost": 224, "customers": []},
		"10_58": { "cost": 188, "customers": []},
		"10_71": { "cost": 204, "customers": [17]},
		"10_73": { "cost": 135, "customers": []},
		"11_7": { "cost": 179, "customers": [7,23]},
		"11_9": { "cost": 170, "customers": [30]},
		"11_26": { "cost": 1387, "customers": []},
		"11_43": { "cost": 225, "customers": []},
		"11_53": { "cost": 318, "customers": [7]},
		"11_57": { "cost": 170, "customers": [23,30]},
		"12_8": { "cost": 81, "customers": [25]},
		"12_9": { "cost": 273, "customers": [25]},
		"12_17": { "cost": 242, "customers": []},
		"12_43": { "cost": 217, "customers": []},
		"12_59": { "cost": 198, "customers": []},
		"12_64": { "cost": 119, "customers": []},
		"13_0": { "cost": 194, "customers": [29]},
		"13_2": { "cost": 109, "customers": []},
		"13_14": { "cost": 77, "customers": [29]},
		"13_34": { "cost": 129, "customers": []},
		"13_49": { "cost": 174, "customers": []},
		"13_66": { "cost": 88, "customers": [29]},
		"14_2": { "cost": 136, "customers": []},
		"14_13": { "cost": 83, "customers": [29]},
		"14_34": { "cost": 92, "customers": []},
		"14_62": { "cost": 161, "customers": []},
		"14_66": { "cost": 108, "customers": []},
		"14_70": { "cost": 291, "customers": [11]},
		"15_1": { "cost": 150, "customers": []},
		"15_5": { "cost": 203, "customers": []},
		"15_17": { "cost": 132, "customers": [34]},
		"15_47": { "cost": 7, "customers": [34]},
		"15_59": { "cost": 169, "customers": [1]},
		"15_61": { "cost": 152, "customers": [1]},
		"16_19": { "cost": 89, "customers": [8,15,22,37]},
		"16_23": { "cost": 125, "customers": [15,22,37]},
		"16_36": { "cost": 148, "customers": [15,22,37]},
		"16_69": { "cost": 96, "customers": [8,15,26,37]},
		"16_70": { "cost": 29, "customers": [8,15,37]},
		"16_72": { "cost": 85, "customers": [8,15,22,26,37]},
		"17_12": { "cost": 243, "customers": []},
		"17_15": { "cost": 115, "customers": [34]},
		"17_47": { "cost": 132, "customers": [34]},
		"17_55": { "cost": 311, "customers": [34]},
		"17_59": { "cost": 144, "customers": [1,34]},
		"17_61": { "cost": 224, "customers": [1,34]},
		"18_4": { "cost": 73, "customers": [4,14,24,35]},
		"18_20": { "cost": 113, "customers": [14,21,35]},
		"18_21": { "cost": 204, "customers": [14,24]},
		"18_25": { "cost": 45, "customers": [14]},
		"18_35": { "cost": 102, "customers": [4,14,35]},
		"18_62": { "cost": 116, "customers": [14]},
		"19_16": { "cost": 87, "customers": [8,15,22,37]},
		"19_23": { "cost": 79, "customers": [22,37]},
		"19_40": { "cost": 112, "customers": []},
		"19_42": { "cost": 85, "customers": [37]},
		"19_69": { "cost": 51, "customers": [8,26,37]},
		"19_72": { "cost": 76, "customers": [15,26,37]},
		"20_4": { "cost": 61, "customers": [4,14,21,24]},
		"20_18": { "cost": 117, "customers": [14,21,35]},
		"20_21": { "cost": 223, "customers": [24]},
		"20_35": { "cost": 42, "customers": [4,21]},
		"20_54": { "cost": 109, "customers": [21,35]},
		"20_68": { "cost": 207, "customers": [21]},
		"21_4": { "cost": 209, "customers": [4,14,24,28]},
		"21_25": { "cost": 199, "customers": [24,28]},
		"21_26": { "cost": 80, "customers": [24,28]},
		"21_45": { "cost": 465, "customers": [28]},
		"21_62": { "cost": 298, "customers": [24]},
		"21_68": { "cost": 8, "customers": [28]},
		"22_23": { "cost": 116, "customers": []},
		"22_36": { "cost": 78, "customers": []},
		"22_48": { "cost": 89, "customers": [16]},
		"22_53": { "cost": 143, "customers": [16]},
		"22_67": { "cost": 114, "customers": [16]},
		"22_72": { "cost": 150, "customers": [16]},
		"23_16": { "cost": 114, "customers": [15,22,37]},
		"23_19": { "cost": 82, "customers": [22,37]},
		"23_22": { "cost": 118, "customers": []},
		"23_36": { "cost": 40, "customers": [22]},
		"23_67": { "cost": 148, "customers": []},
		"23_72": { "cost": 39, "customers": [15,22]},
		"24_29": { "cost": 204, "customers": [12,13]},
		"24_39": { "cost": 143, "customers": [12,13]},
		"24_46": { "cost": 173, "customers": [17]},
		"24_65": { "cost": 164, "customers": [12,33]},
		"24_71": { "cost": 113, "customers": [17]},
		"24_73": { "cost": 149, "customers": [12]},
		"25_4": { "cost": 114, "customers": [4,14,24]},
		"25_18": { "cost": 44, "customers": [14]},
		"25_20": { "cost": 154, "customers": []},
		"25_35": { "cost": 121, "customers": [4]},
		"25_38": { "cost": 340, "customers": []},
		"25_68": { "cost": 175, "customers": []},
		"26_13": { "cost": 667, "customers": []},
		"26_21": { "cost": 82, "customers": [24,28]},
		"26_25": { "cost": 266, "customers": [28]},
		"26_45": { "cost": 479, "customers": [28]},
		"26_57": { "cost": 1242, "customers": []},
		"26_68": { "cost": 93, "customers": [28]},
		"27_34": { "cost": 283, "customers": []},
		"27_40": { "cost": 15, "customers": [19,36]},
		"27_42": { "cost": 47, "customers": [19,36]},
		"27_52": { "cost": 128, "customers": [19]},
		"27_63": { "cost": 131, "customers": []},
		"27_69": { "cost": 82, "customers": [19,36]},
		"28_8": { "cost": 177, "customers": [3,25]},
		"28_9": { "cost": 187, "customers": [9,25,30]},
		"28_33": { "cost": 30, "customers": [3,9]},
		"28_43": { "cost": 123, "customers": [9]},
		"28_57": { "cost": 146, "customers": [9,30]},
		"28_64": { "cost": 161, "customers": []},
		"29_24": { "cost": 206, "customers": [12,13]},
		"29_31": { "cost": 153, "customers": [12,13,33]},
		"29_39": { "cost": 120, "customers": [12,13]},
		"29_65": { "cost": 14, "customers": [12,13,33]},
		"29_73": { "cost": 282, "customers": [12,13]},
		"30_7": { "cost": 110, "customers": [7,23]},
		"30_11": { "cost": 283, "customers": []},
		"30_48": { "cost": 110, "customers": []},
		"30_53": { "cost": 54, "customers": [7]},
		"30_57": { "cost": 209, "customers": [23]},
		"30_67": { "cost": 52, "customers": []},
		"31_22": { "cost": 159, "customers": [16]},
		"31_24": { "cost": 238, "customers": [12,33]},
		"31_29": { "cost": 152, "customers": [12,13,33]},
		"31_30": { "cost": 329, "customers": []},
		"31_39": { "cost": 91, "customers": [13,33]},
		"31_65": { "cost": 161, "customers": [33]},
		"32_1": { "cost": 224, "customers": []},
		"32_3": { "cost": 322, "customers": [18]},
		"32_17": { "cost": 337, "customers": []},
		"32_50": { "cost": 135, "customers": [20]},
		"32_51": { "cost": 142, "customers": [20]},
		"32_55": { "cost": 146, "customers": []},
		"33_9": { "cost": 168, "customers": [3,25,30]},
		"33_28": { "cost": 28, "customers": [3,9]},
		"33_30": { "cost": 189, "customers": []},
		"33_43": { "cost": 125, "customers": [3]},
		"33_57": { "cost": 121, "customers": [9,30]},
		"33_64": { "cost": 196, "customers": [3]},
		"34_4": { "cost": 233, "customers": []},
		"34_14": { "cost": 82, "customers": []},
		"34_41": { "cost": 184, "customers": []},
		"34_44": { "cost": 215, "customers": []},
		"34_62": { "cost": 125, "customers": []},
		"34_66": { "cost": 83, "customers": [6]},
		"35_4": { "cost": 24, "customers": [4,14,24]},
		"35_18": { "cost": 99, "customers": [4,14,35]},
		"35_20": { "cost": 35, "customers": [4,21]},
		"35_21": { "cost": 246, "customers": [4,24]},
		"35_25": { "cost": 129, "customers": [4]},
		"35_54": { "cost": 128, "customers": [4,21,35]},
		"36_19": { "cost": 100, "customers": [37]},
		"36_22": { "cost": 92, "customers": []},
		"36_23": { "cost": 35, "customers": [22]},
		"36_48": { "cost": 148, "customers": []},
		"36_67": { "cost": 118, "customers": []},
		"36_72": { "cost": 66, "customers": [15]},
		"37_6": { "cost": 55, "customers": [27]},
		"37_27": { "cost": 161, "customers": []},
		"37_49": { "cost": 59, "customers": [10,27]},
		"37_60": { "cost": 59, "customers": [10,27]},
		"37_63": { "cost": 21, "customers": [27]},
		"37_71": { "cost": 186, "customers": [17]},
		"38_3": { "cost": 111, "customers": []},
		"38_44": { "cost": 151, "customers": []},
		"38_45": { "cost": 70, "customers": [6]},
		"38_50": { "cost": 326, "customers": [20]},
		"38_51": { "cost": 301, "customers": [20]},
		"38_66": { "cost": 309, "customers": [6]},
		"39_24": { "cost": 153, "customers": [12,13]},
		"39_26": { "cost": 1014, "customers": []},
		"39_29": { "cost": 118, "customers": [12,13]},
		"39_31": { "cost": 84, "customers": [13,33]},
		"39_65": { "cost": 114, "customers": [13,33]},
		"40_2": { "cost": 104, "customers": [36]},
		"40_19": { "cost": 120, "customers": []},
		"40_27": { "cost": 15, "customers": [19,36]},
		"40_42": { "cost": 38, "customers": [19]},
		"40_52": { "cost": 129, "customers": []},
		"40_69": { "cost": 60, "customers": []},
		"41_6": { "cost": 102, "customers": [32]},
		"41_14": { "cost": 141, "customers": []},
		"41_49": { "cost": 117, "customers": [2,32]},
		"41_56": { "cost": 88, "customers": []},
		"41_63": { "cost": 130, "customers": [32]},
		"41_74": { "cost": 40, "customers": [2,32]},
		"42_16": { "cost": 118, "customers": [8,37]},
		"42_19": { "cost": 77, "customers": [37]},
		"42_27": { "cost": 51, "customers": [19,36]},
		"42_40": { "cost": 37, "customers": [19]},
		"42_69": { "cost": 31, "customers": [8,19]},
		"42_70": { "cost": 106, "customers": []},
		"43_9": { "cost": 55, "customers": [25,30]},
		"43_12": { "cost": 195, "customers": []},
		"43_28": { "cost": 139, "customers": [9]},
		"43_33": { "cost": 127, "customers": [3]},
		"43_57": { "cost": 63, "customers": [9,30]},
		"43_64": { "cost": 150, "customers": []},
		"44_1": { "cost": 240, "customers": []},
		"44_3": { "cost": 117, "customers": []},
		"44_38": { "cost": 136, "customers": []},
		"44_51": { "cost": 306, "customers": [20]},
		"44_55": { "cost": 304, "customers": []},
		"44_66": { "cost": 166, "customers": [6]},
		"45_3": { "cost": 197, "customers": []},
		"45_25": { "cost": 297, "customers": []},
		"45_34": { "cost": 304, "customers": [6]},
		"45_38": { "cost": 78, "customers": [6]},
		"45_44": { "cost": 208, "customers": [6]},
		"45_66": { "cost": 359, "customers": [6]},
		"46_27": { "cost": 160, "customers": []},
		"46_37": { "cost": 118, "customers": [17]},
		"46_52": { "cost": 67, "customers": [17]},
		"46_60": { "cost": 63, "customers": []},
		"46_71": { "cost": 101, "customers": [17]},
		"46_73": { "cost": 251, "customers": [17]},
		"47_1": { "cost": 152, "customers": []},
		"47_5": { "cost": 183, "customers": []},
		"47_15": { "cost": 7, "customers": [34]},
		"47_17": { "cost": 129, "customers": [34]},
		"47_51": { "cost": 274, "customers": []},
		"47_61": { "cost": 153, "customers": [1,34]},
		"48_22": { "cost": 89, "customers": [16]},
		"48_30": { "cost": 113, "customers": []},
		"48_31": { "cost": 176, "customers": [33]},
		"48_36": { "cost": 129, "customers": []},
		"48_53": { "cost": 70, "customers": [16]},
		"48_67": { "cost": 84, "customers": []},
		"49_6": { "cost": 16, "customers": [2,10,27]},
		"49_37": { "cost": 60, "customers": [10,27]},
		"49_41": { "cost": 114, "customers": [2,32]},
		"49_60": { "cost": 97, "customers": [10,27]},
		"49_63": { "cost": 38, "customers": [2,10,27,32]},
		"49_74": { "cost": 77, "customers": [2]},
		"50_3": { "cost": 224, "customers": [18]},
		"50_5": { "cost": 102, "customers": [18,20]},
		"50_32": { "cost": 155, "customers": [20]},
		"50_44": { "cost": 299, "customers": [20]},
		"50_51": { "cost": 48, "customers": [20]},
		"50_55": { "cost": 77, "customers": [20]},
		"51_0": { "cost": 401, "customers": []},
		"51_5": { "cost": 58, "customers": [20]},
		"51_32": { "cost": 123, "customers": [20]},
		"51_38": { "cost": 343, "customers": [20]},
		"51_50": { "cost": 49, "customers": [20]},
		"51_55": { "cost": 33, "customers": [20]},
		"52_27": { "cost": 111, "customers": [19]},
		"52_40": { "cost": 129, "customers": []},
		"52_42": { "cost": 132, "customers": [19]},
		"52_46": { "cost": 72, "customers": [17]},
		"52_60": { "cost": 102, "customers": []},
		"52_71": { "cost": 145, "customers": [17]},
		"53_7": { "cost": 137, "customers": [7]},
		"53_30": { "cost": 60, "customers": [7]},
		"53_31": { "cost": 227, "customers": [7,16]},
		"53_48": { "cost": 65, "customers": [16]},
		"53_67": { "cost": 34, "customers": [7,16]},
		"53_72": { "cost": 213, "customers": [16]},
		"54_4": { "cost": 163, "customers": [4,14,21,35]},
		"54_10": { "cost": 246, "customers": []},
		"54_18": { "cost": 239, "customers": [14,21,35]},
		"54_20": { "cost": 95, "customers": [21,35]},
		"54_35": { "cost": 123, "customers": [4,21,35]},
		"54_68": { "cost": 261, "customers": [21,35]},
		"55_5": { "cost": 30, "customers": [18]},
		"55_38": { "cost": 316, "customers": []},
		"55_47": { "cost": 204, "customers": [34]},
		"55_50": { "cost": 67, "customers": [20]},
		"55_51": { "cost": 28, "customers": [20]},
		"55_57": { "cost": 757, "customers": [5]},
		"56_6": { "cost": 140, "customers": []},
		"56_35": { "cost": 152, "customers": []},
		"56_37": { "cost": 149, "customers": []},
		"56_41": { "cost": 84, "customers": []},
		"56_58": { "cost": 110, "customers": []},
		"56_74": { "cost": 77, "customers": []},
		"57_7": { "cost": 183, "customers": [23]},
		"57_9": { "cost": 45, "customers": [9,30]},
		"57_11": { "cost": 170, "customers": [23,30]},
		"57_33": { "cost": 135, "customers": [9,30]},
		"57_39": { "cost": 613, "customers": []},
		"57_43": { "cost": 65, "customers": [9,30]},
		"58_6": { "cost": 110, "customers": []},
		"58_10": { "cost": 159, "customers": []},
		"58_37": { "cost": 77, "customers": []},
		"58_46": { "cost": 152, "customers": [17]},
		"58_56": { "cost": 114, "customers": []},
		"58_63": { "cost": 102, "customers": []},
		"59_8": { "cost": 144, "customers": []},
		"59_12": { "cost": 202, "customers": []},
		"59_17": { "cost": 133, "customers": [1,34]},
		"59_47": { "cost": 153, "customers": [1,34]},
		"59_61": { "cost": 131, "customers": [1]},
		"59_64": { "cost": 141, "customers": []},
		"60_6": { "cost": 113, "customers": [10]},
		"60_37": { "cost": 59, "customers": [10,27]},
		"60_46": { "cost": 68, "customers": []},
		"60_49": { "cost": 98, "customers": [10,27]},
		"60_52": { "cost": 118, "customers": []},
		"60_63": { "cost": 69, "customers": [10]},
		"61_0": { "cost": 78, "customers": [1,29]},
		"61_13": { "cost": 257, "customers": [29]},
		"61_15": { "cost": 144, "customers": [1]},
		"61_17": { "cost": 244, "customers": [1,34]},
		"61_47": { "cost": 147, "customers": [1,34]},
		"61_59": { "cost": 154, "customers": [1]},
		"62_4": { "cost": 148, "customers": [4,14,24]},
		"62_18": { "cost": 102, "customers": [14]},
		"62_25": { "cost": 130, "customers": []},
		"62_34": { "cost": 105, "customers": []},
		"62_35": { "cost": 163, "customers": [4]},
		"62_66": { "cost": 192, "customers": []},
		"63_6": { "cost": 40, "customers": [32]},
		"63_37": { "cost": 25, "customers": [27]},
		"63_41": { "cost": 140, "customers": [32]},
		"63_49": { "cost": 38, "customers": [2,10,27,32]},
		"63_58": { "cost": 98, "customers": []},
		"63_74": { "cost": 91, "customers": [2,32]},
		"64_8": { "cost": 30, "customers": [3]},
		"64_12": { "cost": 112, "customers": []},
		"64_28": { "cost": 158, "customers": []},
		"64_33": { "cost": 195, "customers": [3]},
		"64_43": { "cost": 136, "customers": []},
		"64_59": { "cost": 129, "customers": []},
		"65_29": { "cost": 13, "customers": [12,13,33]},
		"65_31": { "cost": 164, "customers": [33]},
		"65_39": { "cost": 114, "customers": [13,33]},
		"65_48": { "cost": 322, "customers": [33]},
		"65_61": { "cost": 701, "customers": []},
		"65_73": { "cost": 318, "customers": []},
		"66_0": { "cost": 207, "customers": [29]},
		"66_13": { "cost": 84, "customers": [29]},
		"66_14": { "cost": 107, "customers": []},
		"66_34": { "cost": 81, "customers": [6]},
		"66_44": { "cost": 158, "customers": [6]},
		"66_71": { "cost": 434, "customers": [31]},
		"67_7": { "cost": 168, "customers": [7]},
		"67_22": { "cost": 111, "customers": [16]},
		"67_30": { "cost": 54, "customers": []},
		"67_36": { "cost": 114, "customers": []},
		"67_48": { "cost": 75, "customers": []},
		"67_53": { "cost": 37, "customers": [7,16]},
		"68_18": { "cost": 202, "customers": [35]},
		"68_20": { "cost": 196, "customers": [21]},
		"68_21": { "cost": 7, "customers": [28]},
		"68_25": { "cost": 201, "customers": []},
		"68_26": { "cost": 94, "customers": [28]},
		"68_35": { "cost": 213, "customers": []},
		"69_13": { "cost": 220, "customers": []},
		"69_19": { "cost": 59, "customers": [8,26,37]},
		"69_27": { "cost": 79, "customers": [19,36]},
		"69_40": { "cost": 65, "customers": []},
		"69_42": { "cost": 29, "customers": [8,19]},
		"69_70": { "cost": 87, "customers": [8,26]},
		"70_16": { "cost": 31, "customers": [8,15,37]},
		"70_19": { "cost": 84, "customers": [37]},
		"70_40": { "cost": 120, "customers": []},
		"70_42": { "cost": 98, "customers": []},
		"70_69": { "cost": 79, "customers": [8,26]},
		"70_72": { "cost": 107, "customers": [15,26]},
		"71_10": { "cost": 192, "customers": [17]},
		"71_24": { "cost": 105, "customers": [17]},
		"71_39": { "cost": 229, "customers": []},
		"71_46": { "cost": 105, "customers": [17]},
		"71_58": { "cost": 165, "customers": [17]},
		"71_73": { "cost": 137, "customers": [17]},
		"72_19": { "cost": 65, "customers": [15,26,37]},
		"72_22": { "cost": 140, "customers": [16]},
		"72_23": { "cost": 38, "customers": [15,22]},
		"72_36": { "cost": 79, "customers": [15]},
		"72_67": { "cost": 157, "customers": []},
		"72_69": { "cost": 129, "customers": [8,15,26]},
		"73_10": { "cost": 138, "customers": []},
		"73_24": { "cost": 139, "customers": [12]},
		"73_37": { "cost": 283, "customers": []},
		"73_39": { "cost": 311, "customers": [13]},
		"73_65": { "cost": 289, "customers": []},
		"73_71": { "cost": 145, "customers": [17]},
		"74_6": { "cost": 64, "customers": [2]},
		"74_41": { "cost": 41, "customers": [2,32]},
		"74_49": { "cost": 83, "customers": [2]},
		"74_56": { "cost": 91, "customers": []},
		"74_58": { "cost": 119, "customers": []},
		"74_63": { "cost": 98, "customers": [2,32]}
	}
};