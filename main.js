$( document ).ready(function() {
    $("#mynetwork").css({
        "width": (window.innerWidth - 20),
        "height": (window.innerHeight - 50),
        "border": "1px solid lightgray"
    });
    
    $.each(results, function (i, item) {
        $('#instance').append($('<option>', { 
            value: i,
            text : i 
        }));
    });

    $("[name=render]").click(function() {
        redrawAll();
    });

    $("[name=info]").click(function() {
        var name = $("#instance").val();
        
        $("#myModalLabel").text(name);
        
        var instance = getInstance(name);
        
        $("#value").val(instance["value"].toFixed(2));

        if (typeof results[name]["gap"] != "undefined") {
            $("#gap").val(results[name]["gap"]);
        }

        $("#time").val(results[name]["executionTime"]);
    
        console.log(results[name]["executionTime"] + "," + results[name]["gap"] + "," + instance["value"].toFixed(2) + "," +  name);
    });
});


function redrawAll() {
    var name = $("#instance").val();

    var instance = getInstance(name);
   
    var mode = $("#mode").val();

    var nodes = new Array();
    var edges = new Array();

    for (var key in instance.arcs) {
        var aux = key.split("_");
        var times = instance.arcs[key]["times"];

        for (var i = 0; i < 2; ++i) {
            var node = Number(aux[i]);

            var found = false;
            for (var j = 0, tamJ = nodes.length; j < tamJ; ++j) {
                if (nodes[j]["id"] === node) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                if (node === 0) {
                    nodes.push({"id": node, "label": node, "borderWidth": 3, "color": { "border": "#000000", "background": "#ff9980"}});
                } else if (times > 0) {
                    nodes.push({"id": node, "label": node, "color": { "border": "#ff0000"}});
                } else if (mode == "full" || mode == "instance") {
                    nodes.push({"id": node, "label": node});
                }
            }
        }

        var edge = {
            "from": aux[0],
            "to": aux[1],
            "arrows": "to"
        };

        if (times > 0 && (mode == "full" || mode == "solution")) {
            edge["color"] = "red";

            if (times > 1) {
                edge["label"] = times;
            }
        } else if (mode == "full" || mode == "instance") {
            edge["color"] = "blue";
        }

        var label = "";
        for (var i = 0; i < instance.arcs[key].customers.length; i++) {
            if (label != "")
                label += ", ";
            label += instance.arcs[key].customers[i];
        }
        edge["label"] = "(" + instance.arcs[key]["cost"] + ")" + (label ? " [" + label + "]" : "");
        
        edges.push(edge);
    }
    
    var data = {
        nodes: new vis.DataSet(nodes),
        edges: new vis.DataSet(edges)
    };
    
    var container = document.getElementById('mynetwork');
   
    var options = {
        layout: {
            improvedLayout: false
        }
    }

    var network = new vis.Network(container, data, options);
}

function getInstance(name) {
    var instance = instances[name];
    instance["value"] = 0;
    
    var totalCustomers = instance["totalCustomers"];

    var customers = new Array(totalCustomers);

    for (var c = 0; c < totalCustomers; c++) {
        customers[c] = 0;
    }

    var result = results[name];

    for (var key in instance.arcs) {
        instance.arcs[key]["times"] = 0;
    }

    for (var v = 0, tamV = result.route.length; v < tamV; ++v) {
        var previous = -1,
            value = 0;
        for (var i = 0, tamI = result.route[v].length; i < tamI; ++i) {
            var key = result.route[v][i][0] + "_" + result.route[v][i][1];

            instance.arcs[key]["times"]++;

            value += instance.arcs[key]["cost"];

            for (var c = 0, tamC = instance.arcs[key]["customers"].length; c < tamC; ++c) {
                customers[instance.arcs[key]["customers"][c] - 1]++;
            }

            if (previous > -1 && previous != result.route[v][i][0]) {
                alert("FALHA");
            } else {
                previous = result.route[v][i][1];
            }
        }

        if (instance["maxDistance"] < value) {
            console.error("maxDistance: " + instance["maxDistance"] + ", route: " + value + ", name: " + name);
        }

        instance["value"] += value;
    }

    var value = result["cost"];


    if (Math.abs(instance["value"] - result["cost"]) > 1) {
        alert("javascript: " + instance["value"] + ", c++: " + value);
    } else {
        console.log("javascript: " + instance["value"] + ", c++: " + value);
    }
    
    var invalid = 0;
    for (var c = 0; c < totalCustomers; c++) {
        if (customers[c] < 1) {
            console.log("customer " + (c + 1) + " not covered");
            invalid++;
        }
    }
   
    if (invalid > 0) {
        alert("not cover " + invalid + " of " + totalCustomers);
    }

    return instance;
}

function generateStringResults() {
    var keys = Object.keys(results);

    if (keys.length == 0) {
        return "";
    }

    var str = "Method;Constructor;Local Searches\n\n";
    
    if (typeof results[keys[0]]["gap"] != "undefined") {
        str += "Gavish-Graves;;\n";
    } else {
        var localSearches = "";
        for (var i = 0, tam = results[keys[0]]["localSearches"].length; i < tam; ++i) {
            if (localSearches != "") {
                localSearches += " : ";
            }
            localSearches += results[keys[0]]["localSearches"][i];
        }

        str += results[keys[0]]["heuristic"] + ";" + results[keys[0]]["constructor"] + ";" + localSearches + "\n";
    }
    str += "Data;Result;Time\n";

    // sequencia -> 1500, 1000
    var instanceGroups = ["g500-1000_","g500-1500_"];
    var instanceTypes = ["_0.5", "_1", "_5", "_10"];

    for (var group in instanceGroups) {
        for (var type in instanceTypes) {
            for (var i = 0; i < 5; i++) {
                var name = instanceGroups[group] + i + instanceTypes[type];

                if (name in results) {
                    var instance = getInstance(name);
                    str += name + ";" + instance["value"].toFixed(2) + ";" + results[name]["executionTime"] + "\n";
                } else {
                    str += name + ";;\n";
                }
            }
        }
    }
    str += "\n";
    
    instanceGroups = ["MB0537-","MB0547-"];
    instanceTypes = ["0.5-", "1-", "5-", "10-"];

    for (var group in instanceGroups) {
        for (var type in instanceTypes) {
            for (var i = 0; i < 5; i++) {
                var name = instanceGroups[group] + instanceTypes[type] + i;

                if (name in results) {
                    var instance = getInstance(name);
                    str += name + ";" + instance["value"].toFixed(2) + ";" + results[name]["executionTime"] + "\n";
                } else {
                    str += name + ";;\n";
                }
            }
        }
    }
    str += "\n";
    
    instanceGroups = ["UR5","UR7", "UR1"];
    instanceTypes = ["32", "35", "37"];

    for (var group in instanceGroups) {
        for (var i = 0; i < 4; i++) {
            for (var type in instanceTypes) {
                var name = instanceGroups[group] + ((instanceTypes[type] * 1) + (i * 10)) + "_1";

                if (name in results) {
                    var instance = getInstance(name);
                    str += name + ";" + instance["value"].toFixed(2) + ";" + results[name]["executionTime"] + "\n";
                } else {
                    str += name + ";;\n";
                }
            }
        }
    }
    
    //for (var key in keys) {
    //    var name = keys[key];
    //    var instance = getInstance(name);

    //    str += name + ";" + instance["value"].toFixed(2) + ";" + results[name]["executionTime"] + "\n";
    //}

    return str;
}

function generateStringResultsMulti() {
    var keys = Object.keys(results);

    if (keys.length == 0) {
        return "";
    }

    var str = "Method;Constructor;Local Searches\n\n";
    
    if (typeof results[keys[0]]["gap"] != "undefined") {
        str += "Gavish-Graves;;\n";
    } else {
        var localSearches = "";
        for (var i = 0, tam = results[keys[0]]["localSearches"].length; i < tam; ++i) {
            if (localSearches != "") {
                localSearches += " : ";
            }
            localSearches += results[keys[0]]["localSearches"][i];
        }

        str += results[keys[0]]["heuristic"] + ";" + results[keys[0]]["constructor"] + ";" + localSearches + "\n";
    }
    str += "Data;Result;Time\n";

    for (var i = 0; i < keys.length; ++i) {
        var name = keys[i];

        if (name in results) {
            var instance = getInstance(name);
            var aux = instance["value"].toFixed(2) + ";" + results[name]["executionTime"] + "\n";
            str += name + ";" + aux.replace(/\./g, ',');
        } else {
            str += name + ";;\n";
        }
    }

    return str;
}
